﻿## Backend
Unsere SalesOrder Klasse funktioniert nur auf dem Glassfish 4.1.1. Warum ist unklar!
Damit JSON funktioniert muss folgender Fix drin sein: http://stackoverflow.com/questions/33722764/glassfish-error-when-producing-json

* Zu beachten: JPA
- Wir verwenden folgenden Unit-Test um Dummydaten zu erzeugen: FillDB

## Frontend
- Das Frontend liegt unter ./frontend und kann mit 'npm install; npm start' gestartet werden
- Wir haben das Frontend bereits gebuildet unter frontend/dist gebaut
- Alternativ kann es selber gebaut werden:
> ng build ng build --prod --base-href=/bookstore/
> Backend: maven clean install
> Das WAR auf dem Glassfish deployen

# Unsere Kür ist:
- Wir haben ein Modul für Error/Successmeldungen eingebaut (angular2-notifications)
- Wir haben alle Masken implementiert, inkl. Kundenupdate/Bestellungsliste, Bestellung stoppen usw.
- Login und Shopping-cart wird auf Localstorage gespeichert. Kunde ist also nach Neustart der App weiter eingeloggt und Warenkorb bleibt erhalten.
- Auth-Guard um Seiten zu schützen
- Verschachtelte Formulare


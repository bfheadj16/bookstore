package ch.eadj.bookstore;

import ch.eadj.bookstore.application.service.CatalogService;
import ch.eadj.bookstore.application.service.CustomerService;
import ch.eadj.bookstore.application.service.OrderService;
import ch.eadj.bookstore.persistence.entity.*;
import ch.eadj.bookstore.persistence.enumeration.BookBinding;
import ch.eadj.bookstore.persistence.enumeration.CreditCardType;
import ch.eadj.bookstore.persistence.enumeration.Group;
import ch.eadj.bookstore.persistence.enumeration.OrderStatus;
import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Properties;

@Ignore
public class AbstractTest {

    // muss mit jndi enc entry shipmentDurationTime von OrderServiceBean übereinstimmen!
    protected static final Long SHIPMENT_DURATION_TIME = 10000L;

    protected static final String EMAIL_CUSTOMER_1 = "reto.lehmann.2@students.bfh.ch";
    protected static final String EMAIL_CUSTOMER_2 = "adrian.bader@students.bfh.ch";

    private static String getServiceName(String beanName) {
        return "java:global/bookstore-1.0-SNAPSHOT/" + beanName;
    }

    private static EntityManagerFactory emf;
    private static EntityManager em;

    protected static OrderService orderService;
    protected static CatalogService catalogService;
    protected static CustomerService customerService;

    private static Long salesOrderNumber1;
    private static Long salesOrderNumber2;
    private static Long salesOrderNumber3;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

        // Wenn das persistence.xml fürs Testen auch persistence.xml heisst, gibts manchmal Probleme.
        // Deshalb eigener Name verwenden.
        Properties pros = new Properties();
        pros.setProperty(PersistenceUnitProperties.ECLIPSELINK_PERSISTENCE_XML,
                "META-INF/persistence-test.xml");
        emf = Persistence.createEntityManagerFactory("bookstore", pros);
        em = emf.createEntityManager();

//        InitialContext context = new InitialContext();
//        orderService = (OrderService) context.lookup(getServiceName("OrderServiceBean"));
//        catalogService = (CatalogService) context.lookup(getServiceName("CatalogServiceBean"));
//        customerService = (CustomerService) context.lookup(getServiceName("CustomerServiceBean"));
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        if (em != null) {
            em.close();
        }
        if (emf != null) {
            emf.close();
        }
    }

    private void deleteTable(String tableName) {
        try {
            em.getTransaction().begin();

            em.createQuery("DELETE FROM " + tableName).executeUpdate();

            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        }
    }

    @Before
    public void setUp() {
        System.out.println("Cleaning up tables in DB");

        deleteTable("SalesOrderItem");
        deleteTable("SalesOrder");
        deleteTable("Book");
        deleteTable("Customer");
        deleteTable("Login");

        try {
            em.getTransaction().begin();

            // Insert dummy users
            Login reto = new Login(EMAIL_CUSTOMER_1, "123", Group.CUSTOMER);
            Login adrian = new Login(EMAIL_CUSTOMER_2, "123", Group.CUSTOMER);
            Login andreas = new Login("andreas", "123", Group.EMPLOYEE);

            Address andreasAddress = new Address("Bernstrasse 5", "Lyss", "3250", "Schweiz", "BE");
            CreditCard andreasCreditcard = new CreditCard(CreditCardType.MASTERCARD, "4234-4245-4223-5111", 12, 2018);
            Customer andreasCustomer = new Customer("Andreas", "Meyer", EMAIL_CUSTOMER_1, andreas, andreasAddress, andreasCreditcard);

            Address adriansAddress = new Address("Bernstrasse 1", "Bern", "3000", "Schweiz", "BE");
            CreditCard adriansCreditcard = new CreditCard(CreditCardType.VISA, "1234 1245 2223 1111", 11, 2012);
            Customer adrianCustomer = new Customer("Adrian", "Bader", EMAIL_CUSTOMER_2, adrian, adriansAddress, adriansCreditcard);

            Book harryPotter1 = new Book("978-3551559012",
                    "Harry Potter, Band 1: Harry Potter und der Stein der Weisen",
                    "von J.K. Rowling  (Autor), Jim Kay (Illustrator), Klaus Fritz",
                    "Carlsen", 2015, BookBinding.HARDCOVER, 256, BigDecimal.valueOf(26.99));

            Book harryPotter2 = new Book("978-3551559029",
                    "Harry Potter, Band 2: Harry Potter und die Kammer des Schreckens",
                    "von J.K. Rowling  (Autor), Jim Kay (Illustrator), Klaus Fritz",
                    "Carlsen", 2016, BookBinding.HARDCOVER, 272, BigDecimal.valueOf(29.99));

            Book harryPotter3 = new Book("978-3551354037",
                    "Harry Potter und der Gefangene von Askaban",
                    "von J.K. Rowling  (Autor), Jim Kay (Illustrator), Klaus Fritz",
                    "Carlsen", 2007, BookBinding.HARDCOVER, 480, BigDecimal.valueOf(10.99));

            Book todSchweden = new Book("978-3551559013",
                    "Tod in Schweden",
                    "von Kevin Wignall  (Autor), Peter Friedrich (Übersetzer)",
                    "Amazon", 2016, BookBinding.PAPERBACK, 298, BigDecimal.valueOf(9.99));

            Book weissWurst = new Book("978-3423261272",
                    "Weißwurstconnection: Ein Provinzkrimi",
                    "von Rita Falk",
                    "DTV Verlagsgesellschaft", 2013, BookBinding.EBOOK, 304, BigDecimal.valueOf(15.90));


            SalesOrder salesOrder = new SalesOrder(new Date(1479118705000L), OrderStatus.ACCEPTED,
                     adrianCustomer);

            salesOrder.getSalesOrderItems().add(new SalesOrderItem(1, todSchweden, todSchweden.getPrice()));
            salesOrder.getSalesOrderItems().add(new SalesOrderItem(1, harryPotter3, harryPotter3.getPrice()));

            SalesOrder salesOrder2 = new SalesOrder(new Date(1479118605000L), OrderStatus.PROCESSING, adrianCustomer);

            salesOrder2.getSalesOrderItems().add(new SalesOrderItem(1, harryPotter1, harryPotter1.getPrice()));
            salesOrder2.getSalesOrderItems().add(new SalesOrderItem(1, harryPotter2, harryPotter1.getPrice()));

            SalesOrder salesOrder3 = new SalesOrder(new Date(1420156800000L), OrderStatus.SHIPPED, andreasCustomer);

            salesOrder3.getSalesOrderItems().add(new SalesOrderItem(1, weissWurst, weissWurst.getPrice()));
            salesOrder3.getSalesOrderItems().add(new SalesOrderItem(1, harryPotter1, harryPotter1.getPrice()));


            // Login
            em.persist(reto);
            em.persist(adrian);
            em.persist(andreas);

            // Customer
            em.persist(adrianCustomer);
            em.persist(andreasCustomer);

            // Books
            em.persist(harryPotter1);
            em.persist(harryPotter2);
            em.persist(harryPotter3);
            em.persist(weissWurst);
            em.persist(todSchweden);

            // SalesOrder
            em.persist(salesOrder);
            em.persist(salesOrder2);
            em.persist(salesOrder3);

            em.getTransaction().commit();

            salesOrderNumber1 = salesOrder.getNumber();
            salesOrderNumber2 = salesOrder2.getNumber();
            salesOrderNumber3 = salesOrder3.getNumber();

            em.clear();
            emf.getCache().evictAll();

        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        }
    }

    protected static Long getSalesOrderNumber1() {
        return salesOrderNumber1;
    }

    protected static Long getSalesOrderNumber3() {
        return salesOrderNumber3;
    }
}

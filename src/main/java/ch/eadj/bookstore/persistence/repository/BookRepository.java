package ch.eadj.bookstore.persistence.repository;

import ch.eadj.bookstore.persistence.dto.BookInfo;
import ch.eadj.bookstore.persistence.entity.Book;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Stateless
public class BookRepository extends BaseRepository<Book> {

    @Override
    protected Class<Book> getEntityClass() {
        return Book.class;
    }

    public void detachBook(Book book) {
        em.detach(book);
    }

    public List<BookInfo> findByKeywords(List<String> keywords) {

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<BookInfo> cq = cb.createQuery(BookInfo.class);
        Root<Book> book = cq.from(Book.class);

        cq.select(cb.construct(BookInfo.class,
                book.get("isbn"),
                book.get("title"),
                book.get("price")));

        Predicate where = cb.conjunction();
        for (String k : keywords) {
            k = "%" + k.toLowerCase() + "%";

            where = cb.and(where,
                    cb.or(
                            cb.or(
                                    cb.like(cb.lower(book.get("title")), k),
                                    cb.like(cb.lower(book.get("authors")), k)),
                                    cb.like(cb.lower(book.get("publisher")), k)));
        }

        cq.where(where);

        TypedQuery<BookInfo> q = em.createQuery(cq);

        return q.getResultList();
    }
}

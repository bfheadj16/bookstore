package ch.eadj.bookstore.persistence.repository;

import ch.eadj.bookstore.persistence.dto.CustomerInfo;
import ch.eadj.bookstore.persistence.entity.Customer;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import java.util.List;

@Stateless
public class CustomerRepository extends BaseRepository<Customer> {

    @Override
    protected Class<Customer> getEntityClass() {
        return Customer.class;
    }

    public List<CustomerInfo> findByNameOrLastname(String name) {

        TypedQuery<CustomerInfo> query = em.createNamedQuery(Customer.FIND_BY_NAME_OR_LASTNAME, CustomerInfo.class);

        query.setParameter("name", "%" + name.toLowerCase() + "%");

        return query.getResultList();
    }

    public Customer findByEmail(String email) {
        TypedQuery<Customer> query = em.createNamedQuery(Customer.FIND_BY_EMAIL, Customer.class);

        query.setParameter("email", email.toLowerCase());
        Customer customer;

        try {
            customer = query.getSingleResult();
        } catch (Exception ex) {
            return null;
        }

        return customer;
    }
}

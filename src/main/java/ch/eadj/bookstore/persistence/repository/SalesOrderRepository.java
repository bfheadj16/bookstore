package ch.eadj.bookstore.persistence.repository;

import ch.eadj.bookstore.persistence.dto.OrderInfo;
import ch.eadj.bookstore.persistence.dto.OrderStatisticInfo;
import ch.eadj.bookstore.persistence.entity.SalesOrder;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import java.util.List;

@Stateless
public class SalesOrderRepository extends BaseRepository<SalesOrder> {

    @Override
    protected Class<SalesOrder> getEntityClass() {
        return SalesOrder.class;
    }

    public List<OrderInfo> findByCustomerAndOrderYear(Long customerNumber, int year) {

        TypedQuery<OrderInfo> query = em.createNamedQuery(SalesOrder.FIND_BY_CUSTOMER_AND_YEAR, OrderInfo.class);

        query.setParameter("customerNumber", customerNumber);
        query.setParameter("startDate", getStartOfYear(year));
        query.setParameter("endDate", getEndOfYear(year));

        return query.getResultList();
    }

    public List<OrderStatisticInfo> getOrderStatisticsByYear(int year) {
        TypedQuery<OrderStatisticInfo> query = em.createNamedQuery(SalesOrder.STATISTICS_BY_YEAR, OrderStatisticInfo.class);

        query.setParameter("startDate", getStartOfYear(year));
        query.setParameter("endDate", getEndOfYear(year));

        return query.getResultList();
    }
}

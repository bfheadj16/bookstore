package ch.eadj.bookstore.persistence.repository;

import ch.eadj.bookstore.persistence.entity.BaseEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Date;
import java.time.LocalDate;

/**
 * Basis-Repository.
 *
 * @param <T> Die Entity-Klasse
 */
public abstract class BaseRepository<T extends BaseEntity> {

    @PersistenceContext
    protected EntityManager em;

    abstract protected Class<T> getEntityClass();

    protected Date getStartOfYear(int year) {
        LocalDate yearBegin = LocalDate.of(year, 1, 1);
        return Date.valueOf(yearBegin);
    }

    protected Date getEndOfYear(int year) {
        LocalDate yearEnd = LocalDate.of(year, 12, 31);
        return Date.valueOf(yearEnd);
    }

    public T find(Object id) {
        return em.find(getEntityClass(), id);
    }

    public T merge(T entity) {
        return em.merge(entity);
    }

    public void persist(T entity) {
        em.persist(entity);
        em.flush();
    }

    public void remove(T entity) {
        em.remove(entity);
    }
}

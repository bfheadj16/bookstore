package ch.eadj.bookstore.persistence.repository;

import ch.eadj.bookstore.persistence.entity.Login;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

@Stateless
public class LoginRepository extends BaseRepository<Login> {
    @Override
    protected Class<Login> getEntityClass() {
        return Login.class;
    }

    public Login findByEmail(String email) {
        TypedQuery<Login> query = em.createNamedQuery(Login.FIND_BY_EMAIL, Login.class);

        query.setParameter("email", email.toLowerCase());

        Login login;

        try {
            login = query.getSingleResult();
        } catch (Exception ex) {
            return null;
        }

        return login;
    }
}

package ch.eadj.bookstore.persistence.enumeration;

public enum OrderStatus {
    ACCEPTED,
    CANCELED,
    PROCESSING,
    SHIPPED
}

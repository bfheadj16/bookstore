package ch.eadj.bookstore.persistence.enumeration;

public enum CreditCardType {
    MASTERCARD, VISA
}

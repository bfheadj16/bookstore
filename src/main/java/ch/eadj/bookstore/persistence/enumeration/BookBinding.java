package ch.eadj.bookstore.persistence.enumeration;

public enum BookBinding {
    EBOOK, HARDCOVER, PAPERBACK, UNKNOWN
}

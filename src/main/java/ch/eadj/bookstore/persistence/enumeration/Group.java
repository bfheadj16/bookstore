package ch.eadj.bookstore.persistence.enumeration;

public enum Group {
    CUSTOMER, EMPLOYEE
}

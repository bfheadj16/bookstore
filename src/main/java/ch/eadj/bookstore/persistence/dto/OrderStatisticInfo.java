package ch.eadj.bookstore.persistence.dto;

import java.math.BigDecimal;

public class OrderStatisticInfo {
    private BigDecimal totalAmount;
    private Long numberOfPositions;
    private Double averagePrice;
    private CustomerInfo customerInfo;

    public OrderStatisticInfo() {
    }

    public OrderStatisticInfo(BigDecimal totalAmount,
                              Long numberOfPositions,
                              Double averagePrice,
                              Long customerNumber,
                              String customerFirstName,
                              String customerLastName,
                              String customerEmail) {

        this.totalAmount = totalAmount;
        this.numberOfPositions = numberOfPositions;
        this.averagePrice = averagePrice;
        this.customerInfo = new CustomerInfo(customerNumber, customerFirstName, customerLastName, customerEmail, null, null);
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public Long getNumberOfPositions() {
        return numberOfPositions;
    }

    public Double getAveragePrice() {
        return averagePrice;
    }

    public CustomerInfo getCustomerInfo() {
        return customerInfo;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public void setNumberOfPositions(Long numberOfPositions) {
        this.numberOfPositions = numberOfPositions;
    }

    public void setAveragePrice(Double averagePrice) {
        this.averagePrice = averagePrice;
    }

    public void setCustomerInfo(CustomerInfo customerInfo) {
        this.customerInfo = customerInfo;
    }
}

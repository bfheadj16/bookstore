package ch.eadj.bookstore.persistence.dto;

import ch.eadj.bookstore.persistence.entity.Address;
import ch.eadj.bookstore.persistence.entity.CreditCard;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

// implements Serializeable:
// Weil CustomerInfo im Remote-Interface CustomerService als Parameter verwendet wird.
@XmlRootElement
public class CustomerInfo implements Serializable {
    private Long number;
    private String firstName;
    private String lastName;
    private String email;
    private Address address;
    private CreditCard creditCard;

    public CustomerInfo() {
    }

    public CustomerInfo(Long number, String firstName, String lastName, String email, Address address, CreditCard creditCard) {
        this.number = number;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
        this.creditCard = creditCard;
    }

    public Long getNumber() {
        return number;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }
}

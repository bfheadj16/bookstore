package ch.eadj.bookstore.persistence.dto;

import ch.eadj.bookstore.persistence.enumeration.OrderStatus;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

// implements Serializeable:
// Weil OrderInfo im Remote-Interface OrderService verwendet wird.
@XmlRootElement
public class OrderInfo implements Serializable {
    private Long number;
    private Date date;
    private BigDecimal amount;
    private OrderStatus orderStatus;

    public OrderInfo() {
    }

    public OrderInfo(Long number, Date date, BigDecimal amount, OrderStatus orderStatus) {
        this.number = number;
        this.date = date;
        this.amount = amount;
        this.orderStatus = orderStatus;
    }

    public Long getNumber() {
        return number;
    }

    public Date getDate() {
        return date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }
}

package ch.eadj.bookstore.persistence.entity;

import javax.persistence.MappedSuperclass;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;

@MappedSuperclass
@XmlAccessorType(XmlAccessType.FIELD)
public class BaseEntity implements Serializable {
}

package ch.eadj.bookstore.persistence.entity;

import ch.eadj.bookstore.persistence.enumeration.Group;

import javax.persistence.*;

@Entity
@NamedQuery(name = Login.FIND_BY_EMAIL, query = "SELECT l " +
        "FROM Login l " +
        "WHERE LOWER(l.email) = :email")
public class Login extends BaseEntity {

    public static final String FIND_BY_EMAIL = "Login.findByEmail";

    @Id
    @GeneratedValue
    private Long id;

    private String email;

    private String password;

    @Enumerated(EnumType.STRING)
    private Group groupName;

    public Login() {
    }

    public Login(String email, String password, Group groupName) {
        this.email = email;
        this.password = password;
        this.groupName = groupName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String name) {
        this.email = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Group getGroupName() {
        return groupName;
    }

    public void setGroupName(Group groupName) {
        this.groupName = groupName;
    }

    public Long getId() {
        return id;
    }
}

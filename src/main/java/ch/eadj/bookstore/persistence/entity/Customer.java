package ch.eadj.bookstore.persistence.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

// implements Serializeable:
// Weil Customer im Remote-Interface CustomerService/OrderService verwendet wird.
@Entity
@NamedQueries({
        @NamedQuery(name = Customer.FIND_BY_NAME_OR_LASTNAME, query = "SELECT " +
                "new ch.eadj.bookstore.persistence.dto.CustomerInfo(c.number, c.firstName, c.lastName, c.email) FROM Customer c " +
                "WHERE LOWER(c.firstName) LIKE :name OR LOWER(c.lastName) LIKE :name"),

        @NamedQuery(name = Customer.FIND_BY_EMAIL, query = "SELECT c " +
                "FROM Customer c " +
                "WHERE LOWER(c.email) = :email")
})
@XmlRootElement
public class Customer extends BaseEntity {

    public static final String FIND_BY_NAME_OR_LASTNAME = "Customer.findByNameOrLastname";
    public static final String FIND_BY_EMAIL = "Customer.findByEmail";

    @Id
    @GeneratedValue
    private Long number;

    private String firstName;

    private String lastName;

    private String email;

    @Embedded
    private Address address;

    @Embedded
    private CreditCard creditCard;

    public Customer() {
    }

    public Customer(String firstName, String lastName, String email, Login login,
                    Address address, CreditCard creditCard) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
        this.creditCard = creditCard;
    }

    public Long getNumber() {
        return number;
    }

    // Needed to test to create a invalid customer
    public void setNumber(Long number) {
        this.number = number;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }
}

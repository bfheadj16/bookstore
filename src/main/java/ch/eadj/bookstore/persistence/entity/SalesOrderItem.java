package ch.eadj.bookstore.persistence.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;

// implements Serializeable:
// Weil SalesOrderItem im Remote-Interface OrderService verwendet wird.
@Entity
public class SalesOrderItem extends BaseEntity {

    @Id
    @GeneratedValue
    private Long id;

    private Integer quantity;

    private BigDecimal price;

    @ManyToOne(optional = false)
    private Book book;

    public SalesOrderItem() {
    }

    public SalesOrderItem(Integer quantity, Book book, BigDecimal currentPrice) {
        this.quantity = quantity;
        this.price = currentPrice;
        this.book = book;
    }

    public Long getId() {
        return id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
        this.price = book.getPrice();
    }
}

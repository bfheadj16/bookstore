package ch.eadj.bookstore.persistence.entity;


import javax.persistence.Embeddable;
import java.io.Serializable;

// implements Serializeable:
// Weil Address via Customer im Remote-Interface CustomerService als Parameter verwendet wird.
@Embeddable
public class Address implements Serializable {
    private String street;
    private String city;
    private String postalCode;
    private String country;
    private String state;

    public Address() {
    }

    public Address(String street, String city, String postalCode, String country, String state) {
        this.street = street;
        this.city = city;
        this.postalCode = postalCode;
        this.country = country;
        this.state = state;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}

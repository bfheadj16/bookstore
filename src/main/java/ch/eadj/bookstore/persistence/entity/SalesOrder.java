package ch.eadj.bookstore.persistence.entity;

import ch.eadj.bookstore.persistence.dto.CustomerInfo;
import ch.eadj.bookstore.persistence.enumeration.OrderStatus;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

// implements Serializeable:
// Weil SalesOrder im Remote-Interface OrderService als Returnwert verwendet wird.
@Entity
@NamedQueries({
        @NamedQuery(name = SalesOrder.FIND_BY_CUSTOMER_AND_YEAR,
                query = "SELECT new ch.eadj.bookstore.persistence.dto.OrderInfo(o.number, o.date, o.amount, o.orderStatus) " +
                        "FROM SalesOrder o " +
                        "WHERE o.customer.number = :customerNumber AND o.date BETWEEN :startDate AND :endDate"),

        @NamedQuery(name = SalesOrder.STATISTICS_BY_YEAR,
                query = "SELECT new ch.eadj.bookstore.persistence.dto.OrderStatisticInfo(SUM(o.amount), COUNT(oi), AVG(oi.price), c.number, c.firstName, c.lastName, c.email) " +
                        "FROM SalesOrder o " +
                        "JOIN o.customer c " +
                        "JOIN o.salesOrderItems oi " +
                        "WHERE o.date BETWEEN :startDate AND :endDate " +
                        "GROUP BY c")
})
@XmlRootElement
public class SalesOrder extends BaseEntity {

    public static final String FIND_BY_CUSTOMER_AND_YEAR = "SalesOrder.findByCustomerAndYear";
    public static final String STATISTICS_BY_YEAR = "SalesOrder.getStatisticsByYear";

    @Id
    @GeneratedValue
    private Long number;

    private Date date;

    private BigDecimal amount;

    @Enumerated(EnumType.STRING)
    private OrderStatus orderStatus;

    @Embedded
    private Address orderAddress;

    @Embedded
    private CreditCard orderCreditcard;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @XmlTransient
    private Customer customer;

    // Collection-Typ Liste: Sortierung ermoeglichen
    @OneToMany(orphanRemoval = true, cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "SALESORDER_NUMBER")
    private List<SalesOrderItem> salesOrderItems = new ArrayList<>();

    @PrePersist
    public void calculateAmount() {
        this.amount = this.salesOrderItems.stream()
                .map(SalesOrderItem::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public SalesOrder() {
    }

    public SalesOrder(Date date, OrderStatus orderStatus, Customer customer) {
        this.date = date;
        this.orderStatus = orderStatus;
        this.orderAddress = customer.getAddress();
        this.orderCreditcard = customer.getCreditCard();
        this.customer = customer;
    }

    public Long getNumber() {
        return number;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Address getOrderAddress() {
        return orderAddress;
    }

    public void setOrderAddress(Address orderAddress) {
        this.orderAddress = orderAddress;
    }

    public CreditCard getOrderCreditcard() {
        return orderCreditcard;
    }

    public void setOrderCreditcard(CreditCard orderCreditcard) {
        this.orderCreditcard = orderCreditcard;
    }

    public Customer getCustomer() {
        return customer;
    }

    public CustomerInfo getCustomerInfo() {
        if (customer != null)
            return new CustomerInfo(customer.getNumber(), customer.getFirstName(), customer.getLastName(), customer.getEmail(), null, null);
        else
            return null;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<SalesOrderItem> getSalesOrderItems() {
        return salesOrderItems;
    }
}

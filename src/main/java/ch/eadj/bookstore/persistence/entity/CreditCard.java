package ch.eadj.bookstore.persistence.entity;

import ch.eadj.bookstore.persistence.enumeration.CreditCardType;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;

// implements Serializeable:
// Weil CreditCard via Customer im Remote-Interface CustomerService/OrderService verwendet wird.
@Embeddable
public class CreditCard implements Serializable {
    @Enumerated(EnumType.STRING)
    private CreditCardType type;

    @Column(name = "CREDITCARD_NUMBER")
    private String number;

    private Integer expirationMonth;

    private Integer expirationYear;

    public CreditCard() {
    }

    public CreditCard(CreditCardType type, String number, Integer expirationMonth, Integer expirationYear) {
        this.type = type;
        this.number = number;
        this.expirationMonth = expirationMonth;
        this.expirationYear = expirationYear;
    }

    public CreditCardType getType() {
        return type;
    }

    public void setType(CreditCardType type) {
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getExpirationMonth() {
        return expirationMonth;
    }

    public void setExpirationMonth(Integer expirationMonth) {
        this.expirationMonth = expirationMonth;
    }

    public Integer getExpirationYear() {
        return expirationYear;
    }

    public void setExpirationYear(Integer expirationYear) {
        this.expirationYear = expirationYear;
    }
}

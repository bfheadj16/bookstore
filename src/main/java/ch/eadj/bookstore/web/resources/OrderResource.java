package ch.eadj.bookstore.web.resources;

import ch.eadj.bookstore.application.dto.PurchaseOrder;
import ch.eadj.bookstore.application.dto.PurchaseOrderItem;
import ch.eadj.bookstore.application.exception.*;
import ch.eadj.bookstore.application.service.OrderService;
import ch.eadj.bookstore.persistence.dto.OrderInfo;
import ch.eadj.bookstore.persistence.entity.SalesOrder;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

/**
 * Created by User1 on 23.01.2017.
 */
@Path("orders")
@Stateless
public class OrderResource extends BaseResource {
    @EJB
    private OrderService orderService;

    @POST
    @RolesAllowed({"CUSTOMER", "EMPLOYEE"})
    public Response place(PurchaseOrder purchaseOrder) throws PaymentFailedException, BookNotFoundException, CustomerNotFoundException {
        // Check for completeness of input data
        boolean incomplete = purchaseOrder.getCustomerNr() == null
                || purchaseOrder.getItems() == null
                || purchaseOrder.getItems().isEmpty();

        if (!incomplete) {
            // Check every item to have isbn and quantity
            for (PurchaseOrderItem item : purchaseOrder.getItems()) {
                if (item.getBookInfo() == null || item.getQuantity() == null || item.getBookInfo().getIsbn() == null)
                    incomplete = true;
            }
        }
        if (incomplete)
            throw new WebApplicationException("incomplete data",
                    Response.status(BAD_REQUEST).entity("incomplete data").build());

        SalesOrder salesOrder = orderService.placeOrder(purchaseOrder);
        return Response.status(Response.Status.CREATED).entity(salesOrder).build();
    }

    @GET
    @Path("{number}")
    @RolesAllowed({"CUSTOMER", "EMPLOYEE"})
    public SalesOrder findByNumber(@PathParam("number") Long orderNumner) throws OrderNotFoundException {
        return orderService.findOrder(orderNumner);
    }

    @GET
    @RolesAllowed({"CUSTOMER", "EMPLOYEE"})
    public List<OrderInfo> searchByCustomer(@QueryParam("customerNr") Long customerNr, @QueryParam("year") Integer year) throws CustomerNotFoundException, CustomerAlreadyExistsException {
        if (customerNr == null
                || customerNr <= 0
                || year == null
                || year <= 0) {

            throw new WebApplicationException("customer number or year missing",
                    Response.status(BAD_REQUEST).entity("customer number or year missing").build());
        }

        return orderService.searchOrders(customerNr, year);
    }

    @DELETE
    @Path("{number}")
    @RolesAllowed({"CUSTOMER", "EMPLOYEE"})
    public void cancel(@PathParam("number") Long orderNumner) throws OrderNotFoundException, OrderAlreadyShippedException {
        orderService.cancelOrder(orderNumner);
    }
}

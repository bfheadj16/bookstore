package ch.eadj.bookstore.web.resources;

import ch.eadj.bookstore.application.exception.CustomerAlreadyExistsException;
import ch.eadj.bookstore.application.exception.CustomerNotFoundException;
import ch.eadj.bookstore.application.service.CustomerService;
import ch.eadj.bookstore.persistence.dto.CustomerInfo;
import ch.eadj.bookstore.persistence.entity.Customer;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

@Path("customers")
@Stateless
public class CustomerResource extends BaseResource {
    @EJB
    private CustomerService customerService;

    @GET
    @Path("{number}")
    @RolesAllowed({"CUSTOMER"})
    public CustomerInfo findByNumber(@PathParam("number") Long number) throws CustomerNotFoundException {
        Customer customer = customerService.findCustomer(number);

        return new CustomerInfo(customer.getNumber(), customer.getFirstName(),
                customer.getLastName(), customer.getEmail(), customer.getAddress(), customer.getCreditCard());
    }

    @GET
    @RolesAllowed({"CUSTOMER", "EMPLOYEE"})
    public List<CustomerInfo> findByName(@QueryParam("name") String name) {
        return customerService.searchCustomers(name);
    }

    @PUT
    @Path("{number}")
    @RolesAllowed({"CUSTOMER", "EMPLOYEE"})
    public void updateCustomer(Customer customer) throws CustomerNotFoundException, CustomerAlreadyExistsException {
        if (customer.getNumber() == null
                || customer.getAddress() == null
                || customer.getCreditCard() == null
                || customer.getEmail() == null
                || customer.getFirstName() == null
                || customer.getLastName() == null) {

            throw new WebApplicationException("incomplete customer data",
                    Response.status(BAD_REQUEST).entity("incomplete customer data").build());
        }

        customerService.updateCustomer(customer);
    }

}

package ch.eadj.bookstore.web.resources;

import ch.eadj.bookstore.application.dto.Registration;
import ch.eadj.bookstore.application.exception.CustomerAlreadyExistsException;
import ch.eadj.bookstore.application.exception.CustomerNotFoundException;
import ch.eadj.bookstore.application.exception.InvalidPasswordException;
import ch.eadj.bookstore.application.service.CustomerService;
import ch.eadj.bookstore.persistence.entity.Customer;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;
import static javax.ws.rs.core.Response.Status.*;

@Path("registrations")
@Stateless
public class RegistrationResource extends BaseResource {
    @EJB
    private CustomerService customerService;

    @POST
    @Consumes({APPLICATION_JSON})
    @Produces({TEXT_PLAIN})
    @PermitAll
    public Response register(Registration registration) throws CustomerAlreadyExistsException {
        Long customerNr = customerService.registerCustomer(registration);
        return Response.status(CREATED).entity(customerNr).build();
    }

    @GET
    @Path("{email}")
    @Produces({TEXT_PLAIN})
    @PermitAll
    public Response authenticate(@PathParam("email") String email, @HeaderParam("password") String password) throws CustomerNotFoundException, InvalidPasswordException {
        customerService.authenticateCustomer(email, password);
        Customer customer = customerService.findCustomer(email);
        return Response.status(OK).entity(customer.getNumber()).build();
    }

    @PUT
    @Path("{email}")
    @Consumes({TEXT_PLAIN})
    @Produces({TEXT_PLAIN})
    @RolesAllowed({"CUSTOMER", "EMPLOYEE"})
    public Response changePassword(@PathParam("email") String email, String password) throws CustomerNotFoundException {
        customerService.changePassword(email, password);
        Customer customer = customerService.findCustomer(email);
        return Response.status(NO_CONTENT).entity(customer.getNumber()).build();
    }
}

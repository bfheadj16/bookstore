package ch.eadj.bookstore.web.resources;

import ch.eadj.bookstore.application.exception.BookAlreadyExistsException;
import ch.eadj.bookstore.application.exception.BookNotFoundException;
import ch.eadj.bookstore.application.service.CatalogService;
import ch.eadj.bookstore.persistence.dto.BookInfo;
import ch.eadj.bookstore.persistence.entity.Book;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Resource;
import javax.annotation.security.DeclareRoles;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

import static javax.ws.rs.core.Response.Status.*;

@Path("books")
@Stateless
@DeclareRoles({"CUSTOMER", "EMPLOYEE"})
public class CatalogResource extends BaseResource {
    @EJB
    private CatalogService catalogService;

    @Resource
    SessionContext ctx;

    @POST
    @RolesAllowed({"EMPLOYEE"})
    public Response addBook(Book book) throws BookAlreadyExistsException {
        catalogService.addBook(book);
        return Response.status(CREATED).build();
    }

    @GET
    @Path("/{isbn}")
    @PermitAll
    public Book findBook(@PathParam("isbn") String isbn) throws BookNotFoundException {
        return catalogService.findBook(isbn);
    }

    @GET
    @PermitAll
    public List<BookInfo> searchBooks(@QueryParam("keywords") String keywords) {
        if (StringUtils.isEmpty(keywords)) {
            throw new WebApplicationException("keywords are empty",
                    Response.status(BAD_REQUEST).entity("keywords are empty").build());
        }

        return catalogService.searchBooks(keywords);
    }

    @PUT
    @Path("{isbn}")
    @RolesAllowed({"EMPLOYEE"})
    public Response updateBook(Book book, @PathParam("isbn") String isbn) throws BookNotFoundException {
        if (!isbn.equals(book.getIsbn())) {
            throw new WebApplicationException("isbn does not match objects isbn",
                    Response.status(BAD_REQUEST).entity("isbn does not match objects isbn").build());
        }

        catalogService.updateBook(book);
        return Response.status(NO_CONTENT).build();
    }
}

package ch.eadj.bookstore.web;

import ch.eadj.bookstore.web.exceptionhandler.BookstoreExceptionHandler;
import ch.eadj.bookstore.web.filters.CorsFilter;
import ch.eadj.bookstore.web.resources.CatalogResource;
import ch.eadj.bookstore.web.resources.CustomerResource;
import ch.eadj.bookstore.web.resources.OrderResource;
import ch.eadj.bookstore.web.resources.RegistrationResource;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/rest")
public class AppConfig extends Application {
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<>();
        classes.add(BookstoreExceptionHandler.class);
        classes.add(CatalogResource.class);
        classes.add(CustomerResource.class);
        classes.add(OrderResource.class);
        classes.add(RegistrationResource.class);
        classes.add(CorsFilter.class);
        return classes;
    }
}

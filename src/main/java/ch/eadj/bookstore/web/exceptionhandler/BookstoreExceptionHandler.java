package ch.eadj.bookstore.web.exceptionhandler;

import ch.eadj.bookstore.application.exception.*;

import javax.ejb.AccessLocalException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.logging.Level;
import java.util.logging.Logger;

@Provider
public class BookstoreExceptionHandler implements ExceptionMapper<Throwable> {

    private static final Logger logger = Logger.getLogger(BookstoreExceptionHandler.class.getName());

    @Override
    public Response toResponse(Throwable e) {
        Response.Status status = null;

        // Custom WebAppException from our Code
        if (e instanceof WebApplicationException) {
            WebApplicationException webApplicationException = (WebApplicationException) e;
            return webApplicationException.getResponse();
        }

        if (e instanceof AccessLocalException) {
            status = Response.Status.UNAUTHORIZED;
        }

        // Handle BookstoreException & every other exception
        if (e instanceof BookstoreException) {
            if (e instanceof BookAlreadyExistsException) {
                status = Response.Status.CONFLICT;
            } else if (e instanceof BookNotFoundException) {
                status = Response.Status.NOT_FOUND;
            } else if (e instanceof CustomerAlreadyExistsException) {
                status = Response.Status.CONFLICT;
            } else if (e instanceof CustomerNotFoundException) {
                status = Response.Status.NOT_FOUND;
            } else if (e instanceof InvalidPasswordException) {
                status = Response.Status.UNAUTHORIZED;
            } else if (e instanceof OrderAlreadyShippedException) {
                status = Response.Status.FORBIDDEN;
            } else if (e instanceof OrderNotFoundException) {
                status = Response.Status.NOT_FOUND;
            } else if (e instanceof PaymentFailedException) {
                status = Response.Status.PAYMENT_REQUIRED;
            }
        }

        if (status == null) {
            // Unexpected error
            logger.log(Level.SEVERE, e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("").build();
        } else {
            // Application error
            return Response.status(status).entity("").build();
        }
    }
}

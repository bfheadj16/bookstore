package ch.eadj.bookstore.application.dto;

import ch.eadj.bookstore.persistence.dto.BookInfo;

import java.io.Serializable;

// implements Serializeable:
// Weil PurchaseOrderItem im Remote-Interface OrderService verwendet wird.
public class PurchaseOrderItem implements Serializable {
    private BookInfo bookInfo;
    private Integer quantity;

    public PurchaseOrderItem() {
    }

    public PurchaseOrderItem(BookInfo bookInfo, Integer quantity) {
        this.bookInfo = bookInfo;
        this.quantity = quantity;
    }

    public BookInfo getBookInfo() {
        return bookInfo;
    }

    public void setBookInfo(BookInfo bookInfo) {
        this.bookInfo = bookInfo;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}

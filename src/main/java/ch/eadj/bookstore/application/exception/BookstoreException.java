package ch.eadj.bookstore.application.exception;

import javax.ejb.ApplicationException;
import java.io.Serializable;

@ApplicationException(rollback = true)
public abstract class BookstoreException extends Exception implements Serializable {
}

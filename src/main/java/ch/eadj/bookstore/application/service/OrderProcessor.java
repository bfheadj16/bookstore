package ch.eadj.bookstore.application.service;

import ch.eadj.bookstore.persistence.entity.SalesOrder;
import ch.eadj.bookstore.persistence.enumeration.OrderStatus;
import ch.eadj.bookstore.persistence.repository.SalesOrderRepository;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.*;
import java.util.logging.Logger;

@MessageDriven(activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/orderQueue"),
        @ActivationConfigProperty(
                propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class OrderProcessor implements MessageListener {

    private static final Logger logger = Logger.getLogger(OrderProcessor.class.getName());

    @EJB
    private SalesOrderRepository salesOrderRepository;

    @Inject
    @JMSConnectionFactory("jms/connectionFactory")
    private JMSContext jmsContext;

    @Override
    public void onMessage(Message message) {
        logger.info("New message: " + message);

        try {
            TextMessage msg = (TextMessage)message;
            Long orderNumber = Long.parseLong(msg.getText());

            SalesOrder salesOrder = salesOrderRepository.find(orderNumber);

            if (salesOrder == null) {
                logger.severe("salesOrder with id not found: " + orderNumber);
                throw new EJBException();
            }

            salesOrder.setOrderStatus(OrderStatus.PROCESSING);
        } catch (ClassCastException | JMSException e) {
            logger.severe("error in onMessage: " + e.getMessage());
            throw new EJBException(e);
        }
    }
}

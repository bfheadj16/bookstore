package ch.eadj.bookstore.application.service;

import ch.eadj.bookstore.application.dto.Registration;
import ch.eadj.bookstore.application.exception.CustomerAlreadyExistsException;
import ch.eadj.bookstore.application.exception.CustomerNotFoundException;
import ch.eadj.bookstore.application.exception.InvalidPasswordException;
import ch.eadj.bookstore.persistence.dto.CustomerInfo;
import ch.eadj.bookstore.persistence.entity.Customer;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface CustomerService {

    void authenticateCustomer(String email, String password) throws CustomerNotFoundException, InvalidPasswordException;

    void changePassword(String email, String password) throws CustomerNotFoundException;

    Customer findCustomer(Long customerNr) throws CustomerNotFoundException;

    Customer findCustomer(String email) throws CustomerNotFoundException;

    Long registerCustomer(Registration registration) throws CustomerAlreadyExistsException;

    List<CustomerInfo> searchCustomers(String name);

    void updateCustomer(Customer customer) throws CustomerNotFoundException, CustomerAlreadyExistsException;
}

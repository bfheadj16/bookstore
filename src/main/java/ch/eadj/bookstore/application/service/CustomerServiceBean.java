package ch.eadj.bookstore.application.service;

import ch.eadj.bookstore.application.dto.Registration;
import ch.eadj.bookstore.application.exception.CustomerAlreadyExistsException;
import ch.eadj.bookstore.application.exception.CustomerNotFoundException;
import ch.eadj.bookstore.application.exception.InvalidPasswordException;
import ch.eadj.bookstore.persistence.dto.CustomerInfo;
import ch.eadj.bookstore.persistence.entity.Customer;
import ch.eadj.bookstore.persistence.entity.Login;
import ch.eadj.bookstore.persistence.enumeration.Group;
import ch.eadj.bookstore.persistence.repository.CustomerRepository;
import ch.eadj.bookstore.persistence.repository.LoginRepository;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class CustomerServiceBean implements CustomerService {

    @EJB
    private CustomerRepository customerRepository;

    @EJB
    private LoginRepository loginRepository;

    @Override
    public void authenticateCustomer(String email, String password) throws CustomerNotFoundException, InvalidPasswordException {
        Login customerLogin = loginRepository.findByEmail(email);

        if (customerLogin == null) {
            throw new CustomerNotFoundException();
        }

        if (!customerLogin.getPassword().equals(password)) {
            throw new InvalidPasswordException();
        }
    }

    @Override
    public void changePassword(String email, String password) throws CustomerNotFoundException {
        Login customerLogin = loginRepository.findByEmail(email);

        if (customerLogin == null) {
            throw new CustomerNotFoundException();
        }

        customerLogin.setPassword(password);
    }

    @Override
    public Customer findCustomer(Long customerNr) throws CustomerNotFoundException {
        Customer customer = customerRepository.find(customerNr);

        if (customer == null) {
            throw new CustomerNotFoundException();
        }

        return customer;
    }

    @Override
    public Customer findCustomer(String email) throws CustomerNotFoundException {
        Customer customer = customerRepository.findByEmail(email);

        if (customer == null) {
            throw new CustomerNotFoundException();
        }

        return customer;
    }

    @Override
    public Long registerCustomer(Registration registration) throws CustomerAlreadyExistsException {

        Customer existingCustomer = customerRepository.findByEmail(registration.getCustomer().getEmail());

        if (existingCustomer != null) {
            throw new CustomerAlreadyExistsException();
        }

        Login newLogin = new Login(registration.getCustomer().getEmail(), registration.getPassword(), Group.CUSTOMER);
        loginRepository.persist(newLogin);
        customerRepository.persist(registration.getCustomer());

        return registration.getCustomer().getNumber();
    }


    @Override
    public List<CustomerInfo> searchCustomers(String name) {
        return customerRepository.findByNameOrLastname(name);
    }

    @Override
    public void updateCustomer(Customer customer) throws CustomerNotFoundException, CustomerAlreadyExistsException {
        Customer customerFromDb = customerRepository.find(customer.getNumber());

        if (customerFromDb == null) {
            throw new CustomerNotFoundException();
        }

        Customer otherCustomerWithSameEmail = customerRepository.findByEmail(customer.getEmail());

        if (otherCustomerWithSameEmail != null && !customerFromDb.getNumber().equals(otherCustomerWithSameEmail.getNumber())) {
            throw new CustomerAlreadyExistsException();
        }

        if (!customerFromDb.getEmail().equals(customer.getEmail())) {
            Login loginForCustomer = loginRepository.findByEmail(customerFromDb.getEmail());

            loginForCustomer.setEmail(customer.getEmail());
        }

        customerRepository.merge(customer);
    }
}

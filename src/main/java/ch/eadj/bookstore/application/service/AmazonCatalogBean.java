package ch.eadj.bookstore.application.service;

import ch.eadj.bookstore.application.exception.BookNotFoundException;
import ch.eadj.bookstore.persistence.dto.BookInfo;
import ch.eadj.bookstore.persistence.entity.Book;
import ch.eadj.bookstore.persistence.enumeration.BookBinding;
import com.amazon.webservices.awsecommerceservice._2011_08_01.*;

import javax.ejb.Stateful;
import javax.jws.HandlerChain;
import javax.xml.ws.WebServiceRef;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Stateful
public class AmazonCatalogBean {
    private static final String ASSOCIATE_TAG = "test0e5d-20";
    private static final String BOOK_INDEX = "Books";
    private static final String ISBN = "ISBN";
    private static final String RESPONSE_GROUP_MEDIUM = "Medium";
    private static final String BINDING_HARDCOVER = "HARDCOVER";

    @WebServiceRef(AWSECommerceService.class)
    @HandlerChain(file = "soap/handler-chain.xml")        // AWSSecurityHandler
    private AWSECommerceServicePortType awsService;

    public List<BookInfo> getBooksByKeywords(String keywords) {
        // Prepare request
        ItemSearch itemSearch = new ItemSearch();
        itemSearch.setAssociateTag(ASSOCIATE_TAG);
        ItemSearchRequest searchRequest = new ItemSearchRequest();
        searchRequest.setSearchIndex(BOOK_INDEX);
        searchRequest.setKeywords(keywords);
        searchRequest.getResponseGroup().add(0, RESPONSE_GROUP_MEDIUM);
        searchRequest.setIncludeReviewsSummary("false");
        itemSearch.getRequest().add(0, searchRequest);

        // Send request
        ItemSearchResponse searchResponse = awsService.itemSearch(itemSearch);

        return searchResponse.getItems()
                .stream()
                .flatMap(i -> i.getItem().stream())
                .filter(i -> i.getItemAttributes() != null &&
                        i.getItemAttributes().getListPrice() != null && i.getItemAttributes().getListPrice().getAmount().longValue() > 0L)
                .map(i -> getBookInfoFromAWSItem(i.getItemAttributes()))
                .collect(Collectors.toList());
    }

    public Book getBookByISBN(String isbn) throws BookNotFoundException {
        // Prepare request
        ItemLookup itemLookup = new ItemLookup();
        itemLookup.setAssociateTag(ASSOCIATE_TAG);
        ItemLookupRequest lookupRequest = new ItemLookupRequest();
        lookupRequest.getResponseGroup().add(0, RESPONSE_GROUP_MEDIUM);
        lookupRequest.setIdType(ISBN);
        lookupRequest.setSearchIndex(BOOK_INDEX);
        lookupRequest.setIncludeReviewsSummary("false");
        lookupRequest.getItemId().add(0, isbn);
        itemLookup.getRequest().add(0, lookupRequest);

        // Send request
        ItemLookupResponse lookupResponse = awsService.itemLookup(itemLookup);

        // Parse book out of response
        Item awsBookItem = null;

        // Loop through lookupResponse.Item(0).Items and make sure to return only items with isbn matching search isbn.
        // There are also items returned with eisbn instead of isbn!
        if (lookupResponse.getItems() != null &&
                lookupResponse.getItems().size() > 0 &&
                lookupResponse.getItems().get(0).getItem() != null)
            awsBookItem = lookupResponse.getItems().get(0).getItem()
                    .stream()
                    .filter(i -> i.getItemAttributes() != null && Objects.equals(i.getItemAttributes().getISBN(), isbn))
                    .findFirst()
                    .orElse(null);

        if (awsBookItem != null)
            return getBookFromAWSItem(awsBookItem.getItemAttributes());
        else
            throw new BookNotFoundException();
    }

    private BookInfo getBookInfoFromAWSItem(ItemAttributes awsBookAttributes) {


        return new BookInfo(awsBookAttributes.getISBN(),
                awsBookAttributes.getTitle(),
                getAWSPrice(awsBookAttributes));
    }

    private Book getBookFromAWSItem(ItemAttributes awsBookAttributes) {
        String authors = awsBookAttributes.getAuthor().stream().collect(Collectors.joining(", "));
        int publicationYear = Integer.parseInt(awsBookAttributes.getPublicationDate().substring(0, 4));

        BookBinding bookBinding = awsBookAttributes.getBinding().equals(BINDING_HARDCOVER) ? BookBinding.HARDCOVER : BookBinding.UNKNOWN;

        return new Book(awsBookAttributes.getISBN(),
                awsBookAttributes.getTitle(),
                authors,
                awsBookAttributes.getPublisher(),
                publicationYear,
                bookBinding,
                awsBookAttributes.getNumberOfPages() != null ? awsBookAttributes.getNumberOfPages().intValue() : 0,
                getAWSPrice(awsBookAttributes));
    }

    private BigDecimal getAWSPrice(ItemAttributes awsBookAttributes) {
        BigDecimal price = BigDecimal.ZERO;
        if (awsBookAttributes != null && awsBookAttributes.getListPrice() != null
                && awsBookAttributes.getListPrice().getAmount() != null) {
            price = new BigDecimal(awsBookAttributes.getListPrice().getAmount()).divide(BigDecimal.valueOf(100));
        }
        return price;
    }
}

package ch.eadj.bookstore.application.service;

import ch.eadj.bookstore.application.dto.PurchaseOrder;
import ch.eadj.bookstore.application.exception.*;
import ch.eadj.bookstore.persistence.dto.OrderInfo;
import ch.eadj.bookstore.persistence.entity.SalesOrder;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface OrderService {
    void cancelOrder(Long orderNr) throws OrderNotFoundException, OrderAlreadyShippedException;

    SalesOrder findOrder(Long orderNr) throws OrderNotFoundException;

    SalesOrder placeOrder(PurchaseOrder purchaseOrder) throws CustomerNotFoundException, BookNotFoundException, PaymentFailedException;

    List<OrderInfo> searchOrders(Long customerNr, Integer year) throws CustomerNotFoundException;
}

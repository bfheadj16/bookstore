package ch.eadj.bookstore.application.service;

import ch.eadj.bookstore.application.dto.PurchaseOrder;
import ch.eadj.bookstore.application.dto.PurchaseOrderItem;
import ch.eadj.bookstore.application.exception.*;
import ch.eadj.bookstore.persistence.dto.OrderInfo;
import ch.eadj.bookstore.persistence.entity.Book;
import ch.eadj.bookstore.persistence.entity.Customer;
import ch.eadj.bookstore.persistence.entity.SalesOrder;
import ch.eadj.bookstore.persistence.entity.SalesOrderItem;
import ch.eadj.bookstore.persistence.enumeration.OrderStatus;
import ch.eadj.bookstore.persistence.repository.BookRepository;
import ch.eadj.bookstore.persistence.repository.CustomerRepository;
import ch.eadj.bookstore.persistence.repository.SalesOrderRepository;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.inject.Inject;
import javax.jms.*;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Logger;

@Stateless
public class OrderServiceBean implements OrderService {

    @EJB
    private SalesOrderRepository salesOrderRepository;

    @EJB
    private CustomerRepository customerRepository;

    @EJB
    private BookRepository bookRepository;

    @EJB
    private AmazonCatalogBean amazonCatalogBean;

    @Inject
    @JMSConnectionFactory("jms/connectionFactory")
    private JMSContext jmsContext;

    @Resource(lookup = "jms/orderQueue")
    private Queue queue;

    @Resource
    private TimerService timerService;

    private static final Logger logger = Logger.getLogger(OrderServiceBean.class.getName());

    private final Long shipmentDurationTime = 10000l;

    @Override
    public void cancelOrder(Long orderNr) throws OrderNotFoundException, OrderAlreadyShippedException {
        SalesOrder existingSalesOrder = salesOrderRepository.find(orderNr);

        if (existingSalesOrder == null) {
            throw new OrderNotFoundException();
        }

        if (existingSalesOrder.getOrderStatus() == OrderStatus.SHIPPED) {
            throw new OrderAlreadyShippedException();
        }

        existingSalesOrder.setOrderStatus(OrderStatus.CANCELED);
    }

    @Override
    public SalesOrder findOrder(Long orderNr) throws OrderNotFoundException {
        SalesOrder existingSalesOrder = salesOrderRepository.find(orderNr);

        if (existingSalesOrder == null) {
            throw new OrderNotFoundException();
        }

        return existingSalesOrder;
    }

    @Override
        public SalesOrder placeOrder(PurchaseOrder purchaseOrder) throws CustomerNotFoundException, BookNotFoundException,
            PaymentFailedException {
        Customer customer = customerRepository.find(purchaseOrder.getCustomerNr());

        if (customer == null) {
            throw new CustomerNotFoundException();
        }

        int year = customer.getCreditCard().getExpirationYear();
        int month = customer.getCreditCard().getExpirationMonth();

        if (year < Calendar.getInstance().get(Calendar.YEAR)
                ||
                (year == Calendar.getInstance().get(Calendar.YEAR)
                        && month < Calendar.getInstance().get(Calendar.MONTH))
                ) {
            throw new PaymentFailedException();
        }

        SalesOrder newSalesOrder = new SalesOrder(new Date(new java.util.Date().getTime()), OrderStatus.ACCEPTED, customer);
        for (PurchaseOrderItem item : purchaseOrder.getItems()) {

            Book book = bookRepository.find(item.getBookInfo().getIsbn());

            if (book == null) {
                // Buch ist noch nicht in der DB > von AWS holen & speichern
                book = getAndSaveBook(item.getBookInfo().getIsbn());
            }

            SalesOrderItem newItem = new SalesOrderItem(item.getQuantity(), book, item.getBookInfo().getPrice());

            newSalesOrder.getSalesOrderItems().add(newItem);
        }

        salesOrderRepository.persist(newSalesOrder);

        // Add to order-processor
        putOrderInQueue(newSalesOrder.getNumber());

        // Deliver order
        timerService.createSingleActionTimer(shipmentDurationTime, new TimerConfig(newSalesOrder.getNumber(), true));

        return newSalesOrder;
    }

    private Book getAndSaveBook(String isbn) throws BookNotFoundException {
        logger.info("getting book from aws: " + isbn);

        Book bookToSave = amazonCatalogBean.getBookByISBN(isbn);

        bookRepository.persist(bookToSave);

        return bookToSave;
    }

    @Timeout
    public void shipOrderTimeout(Timer timer) {
        Long salesOrderNumber = (Long) timer.getInfo();
        logger.info("Timer called. Now shipping order: " + salesOrderNumber);

        SalesOrder salesOrder = salesOrderRepository.find(salesOrderNumber);
        if (salesOrder != null && salesOrder.getOrderStatus() != OrderStatus.CANCELED) {
            salesOrder.setOrderStatus(OrderStatus.SHIPPED);
        } else {
            logger.info("Order is cancelled or deleted and cannot be shipped");
        }
    }

    @Override
    public List<OrderInfo> searchOrders(Long customerNr, Integer year) throws CustomerNotFoundException {
        Customer customer = customerRepository.find(customerNr);
        if (customer == null) {
            throw new CustomerNotFoundException();
        }

        return salesOrderRepository.findByCustomerAndOrderYear(customerNr, year);
    }

    private void putOrderInQueue(Long salesOrderNumber) {
        JMSProducer producer = jmsContext.createProducer();
        TextMessage msg = jmsContext.createTextMessage(salesOrderNumber.toString());
        producer.send(queue, msg);
    }
}

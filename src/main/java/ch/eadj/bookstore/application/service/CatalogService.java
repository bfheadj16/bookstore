package ch.eadj.bookstore.application.service;

import ch.eadj.bookstore.application.exception.BookAlreadyExistsException;
import ch.eadj.bookstore.application.exception.BookNotFoundException;
import ch.eadj.bookstore.persistence.dto.BookInfo;
import ch.eadj.bookstore.persistence.entity.Book;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface CatalogService {

    void addBook(Book book) throws BookAlreadyExistsException;

    Book findBook(String isbn) throws BookNotFoundException;

    List<BookInfo> searchBooks(String keywords);

    void updateBook(Book book) throws BookNotFoundException;
}

package ch.eadj.bookstore.application.service;

import ch.eadj.bookstore.application.exception.BookAlreadyExistsException;
import ch.eadj.bookstore.application.exception.BookNotFoundException;
import ch.eadj.bookstore.persistence.dto.BookInfo;
import ch.eadj.bookstore.persistence.entity.Book;
import ch.eadj.bookstore.persistence.repository.BookRepository;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class CatalogServiceBean implements CatalogService {

    @EJB
    private BookRepository bookRepository;

    @EJB
    private AmazonCatalogBean amazonCatalogBean;

    @Override
    public void addBook(Book book) throws BookAlreadyExistsException {

        Book existingBook = bookRepository.find(book.getIsbn());

        if (existingBook != null) {
            throw new BookAlreadyExistsException();
        }

        bookRepository.persist(book);
    }

    @Override
    public Book findBook(String isbn) throws BookNotFoundException {
        Book book = bookRepository.find(isbn);

        if (book == null) {
            // Search Amazon
            book = amazonCatalogBean.getBookByISBN(isbn);
        }

        if (book == null) {
            throw new BookNotFoundException();
        }

        return book;
    }

    @Override
    public List<BookInfo> searchBooks(String keywords) {
        return amazonCatalogBean.getBooksByKeywords(keywords);
    }

    @Override
    public void updateBook(Book book) throws BookNotFoundException {
        Book existingBook = bookRepository.find(book.getIsbn());

        if (existingBook == null) {
            throw new BookNotFoundException();
        }

        bookRepository.merge(book);
    }
}

import {Injectable} from '@angular/core';
import {SalesOrder} from '../models/salesorder';
import {BaseService, BACKEND_URL} from './base.service';
import {Http, Headers} from '@angular/http';
import {NotificationsService} from 'angular2-notifications';
import {AuthService} from './auth.service';

@Injectable()
export class CustomerService extends BaseService {
    private orders: SalesOrder[] = [];

    constructor(protected http: Http, private notificationService: NotificationsService,
                private authService: AuthService) {
        super(http, notificationService);
    }

    public getOrdersByYear(year: number): Promise<SalesOrder[]> {
        return this.http.get(`${BACKEND_URL}/orders?customerNr=${this.authService.customer.number}&year=${year}`).toPromise()
            .then(res => res.json() as SalesOrder[])
            .catch(this.handleError);
    }

    public getOrderByNumber(number: number): Promise<SalesOrder> {
        return this.http.get(`${BACKEND_URL}/orders/${number}`).toPromise()
            .then(res => res.json() as SalesOrder)
            .catch(this.handleError);
    }

    public registerCustomer(registration: any): Promise<number> {
        let headers = new Headers({"Accept": "text/plain"});
        return this.http.post(`${BACKEND_URL}/registrations`, registration, {headers: headers}).toPromise()
            .then(res => res.text())
            .catch(err => this.handleError);
    }

    public updateCustomerData(): void {
        let customerData = this.authService.customer;
        this.http.put(`${BACKEND_URL}/customers/${customerData.number}`,
            customerData, {headers: this.getHeadersAcceptJson()}).toPromise()
            .then(() => {
                this.authService.updateLocalstorage();
                this.notificationService.success("Updated", "Customer data updated");
            })
            .catch(this.handleError);
    }
}
import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import {NotificationsService} from 'angular2-notifications';

export const BACKEND_URL: String = "http://localhost:8080/bookstore/rest";
//export const BACKEND_URL: String = "http://distsys.ch:8080/bookstore/rest";

@Injectable()
export class BaseService {

    private headersAcceptJson = new Headers({"Accept": "application/json"});

    constructor(protected http: Http, private notificationServie: NotificationsService) {}

    protected handleError(err: any) {
        this.notificationServie.error('Error while fetching backend', err);
        return Promise.reject(err);
    }

    protected getHeadersAcceptJson() {
        return this.headersAcceptJson;
    }
}
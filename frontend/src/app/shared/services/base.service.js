"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var angular2_notifications_1 = require('angular2-notifications');
exports.BACKEND_URL = "http://localhost:8080/bookstore/rest";
//export const BACKEND_URL: String = "http://distsys.ch:8080/bookstore/rest";
var BaseService = (function () {
    function BaseService(http, notificationServie) {
        this.http = http;
        this.notificationServie = notificationServie;
        this.headersAcceptJson = new http_1.Headers({ "Accept": "application/json" });
    }
    BaseService.prototype.handleError = function (err) {
        this.notificationServie.error('Error while fetching backend', err);
        return Promise.reject(err);
    };
    BaseService.prototype.getHeadersAcceptJson = function () {
        return this.headersAcceptJson;
    };
    BaseService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, angular2_notifications_1.NotificationsService])
    ], BaseService);
    return BaseService;
}());
exports.BaseService = BaseService;
//# sourceMappingURL=base.service.js.map
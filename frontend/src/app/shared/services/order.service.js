"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var shopping_cart_1 = require("../models/shopping-cart");
var angular2_notifications_1 = require("angular2-notifications");
var core_1 = require("@angular/core");
var base_service_1 = require("./base.service.ts");
var http_1 = require("@angular/http");
var auth_service_1 = require("./auth.service.ts");
var OrderService = (function (_super) {
    __extends(OrderService, _super);
    function OrderService(authService, http, notificationService) {
        _super.call(this, http, notificationService);
        this.authService = authService;
        this.http = http;
        this.notificationService = notificationService;
        this.shoppingCart = new shopping_cart_1.default(notificationService);
    }
    OrderService.prototype.order = function () {
        var _this = this;
        return this.http.post(base_service_1.BACKEND_URL + "/orders", {
            customerNr: this.authService.customer.number,
            items: this.shoppingCart.items
        }, {
            headers: this.getHeadersAcceptJson()
        }).toPromise()
            .then(function (response) {
            _this.shoppingCart.clear();
            return response.json();
        })
            .catch(function (error) { return _this.handleError(error); });
    };
    OrderService.prototype.cancelOrder = function (orderId) {
        var _this = this;
        return this.http.delete(base_service_1.BACKEND_URL + "/orders/" + orderId).toPromise()
            .then(function (res) { return _this.notificationService.success("Canceled", "Order has been cancelled"); })
            .catch(this.handleError);
    };
    Object.defineProperty(OrderService.prototype, "cart", {
        get: function () {
            return this.shoppingCart;
        },
        enumerable: true,
        configurable: true
    });
    OrderService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [auth_service_1.AuthService, http_1.Http, angular2_notifications_1.NotificationsService])
    ], OrderService);
    return OrderService;
}(base_service_1.BaseService));
exports.OrderService = OrderService;
//# sourceMappingURL=order.service.js.map

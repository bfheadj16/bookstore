"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var customer_1 = require('../models/customer');
var router_1 = require('@angular/router');
var base_service_1 = require('./base.service.ts');
var http_1 = require('@angular/http');
var angular2_notifications_1 = require('angular2-notifications');
var LOCALSTORAGE_KEY = 'customer';
var AuthService = (function (_super) {
    __extends(AuthService, _super);
    function AuthService(router, http, notificationService) {
        _super.call(this, http, notificationService);
        this.router = router;
        this.http = http;
        this.notificationService = notificationService;
        this._customer = null;
        var data = window.localStorage.getItem(LOCALSTORAGE_KEY);
        if (data) {
            this._customer = JSON.parse(data);
        }
    }
    AuthService.prototype.updateLocalstorage = function () {
        window.localStorage.setItem(LOCALSTORAGE_KEY, JSON.stringify(this._customer));
    };
    AuthService.prototype.login = function (username, password) {
        var _this = this;
        this._customer = new customer_1.Customer();
        var headers = new http_1.Headers({ "Accept": "text/plain" });
        headers.append('password', password);
        this.http.get(base_service_1.BACKEND_URL + "/registrations/" + username, { headers: headers }).toPromise()
            .then(function (response) {
            // Login ok, get user-data
            var custId = response.text();
            _this.http.get(base_service_1.BACKEND_URL + "/customers/" + custId, { headers: _this.getHeadersAcceptJson() }).toPromise()
                .then(function (res) {
                _this._customer = res.json();
                // Save to local storage
                _this.updateLocalstorage();
                _this.router.navigate(['/catalog']);
            })
                .catch(function (error) { return _this.handleError(error); });
        })
            .catch(function (error) { return _this.handleError(error); });
    };
    AuthService.prototype.logout = function () {
        this._customer = null;
        window.localStorage.removeItem(LOCALSTORAGE_KEY);
        this.router.navigate(['/catalog']);
    };
    AuthService.prototype.isLoggedIn = function () {
        return this._customer != null;
    };
    Object.defineProperty(AuthService.prototype, "customer", {
        get: function () {
            return this._customer;
        },
        enumerable: true,
        configurable: true
    });
    AuthService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [router_1.Router, http_1.Http, angular2_notifications_1.NotificationsService])
    ], AuthService);
    return AuthService;
}(base_service_1.BaseService));
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map

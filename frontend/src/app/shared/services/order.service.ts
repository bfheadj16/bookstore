import ShoppingCart from "../models/shopping-cart";
import {NotificationsService} from "angular2-notifications";
import {Injectable} from "@angular/core";
import {BaseService, BACKEND_URL} from "./base.service";
import {Http} from "@angular/http";
import {AuthService} from "./auth.service";

@Injectable()
export class OrderService extends BaseService {
    private shoppingCart: ShoppingCart;

    constructor(private authService: AuthService, protected http: Http, private notificationService: NotificationsService) {
        super(http, notificationService);
        this.shoppingCart = new ShoppingCart(notificationService);
    }

    public order() {

        return this.http.post(`${BACKEND_URL}/orders`, {
            customerNr: this.authService.customer.number,
            items: this.shoppingCart.items
        }, {
            headers: this.getHeadersAcceptJson()
        }).toPromise()
            .then(response => {
                this.shoppingCart.clear();
                return response.json()
            })
            .catch(error => this.handleError(error));
    }

    public cancelOrder(orderId: number): Promise<any> {
        return this.http.delete(`${BACKEND_URL}/orders/${orderId}`).toPromise()
            .then(res => this.notificationService.success("Canceled", "Order has been cancelled"))
            .catch(this.handleError);
    }

    get cart() {
        return this.shoppingCart;
    }
}
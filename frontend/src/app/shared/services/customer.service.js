"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var base_service_1 = require('./base.service.ts');
var http_1 = require('@angular/http');
var angular2_notifications_1 = require('angular2-notifications');
var auth_service_1 = require('./auth.service.ts');
var CustomerService = (function (_super) {
    __extends(CustomerService, _super);
    function CustomerService(http, notificationService, authService) {
        _super.call(this, http, notificationService);
        this.http = http;
        this.notificationService = notificationService;
        this.authService = authService;
        this.orders = [];
    }
    CustomerService.prototype.getOrdersByYear = function (year) {
        return this.http.get(base_service_1.BACKEND_URL + "/orders?customerNr=" + this.authService.customer.number + "&year=" + year).toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    CustomerService.prototype.getOrderByNumber = function (number) {
        return this.http.get(base_service_1.BACKEND_URL + "/orders/" + number).toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    CustomerService.prototype.registerCustomer = function (registration) {
        var _this = this;
        var headers = new http_1.Headers({ "Accept": "text/plain" });
        return this.http.post(base_service_1.BACKEND_URL + "/registrations", registration, { headers: headers }).toPromise()
            .then(function (res) { return res.text(); })
            .catch(function (err) { return _this.handleError; });
    };
    CustomerService.prototype.updateCustomerData = function () {
        var _this = this;
        var customerData = this.authService.customer;
        this.http.put(base_service_1.BACKEND_URL + "/customers/" + customerData.number, customerData, { headers: this.getHeadersAcceptJson() }).toPromise()
            .then(function () {
            _this.authService.updateLocalstorage();
            _this.notificationService.success("Updated", "Customer data updated");
        })
            .catch(this.handleError);
    };
    CustomerService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, angular2_notifications_1.NotificationsService, auth_service_1.AuthService])
    ], CustomerService);
    return CustomerService;
}(base_service_1.BaseService));
exports.CustomerService = CustomerService;
//# sourceMappingURL=customer.service.js.map

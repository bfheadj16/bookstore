import {Book} from '../models/book';
import {Injectable} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {BaseService, BACKEND_URL} from './base.service';

@Injectable()
export class CatalogService extends BaseService {

    public findBook(isbn: String): Promise<Book> {
        return this.http.get(`${BACKEND_URL}/books/${isbn}`, {headers: this.getHeadersAcceptJson()}).toPromise()
            .then(response => response.json() as Book)
            .catch(error => this.handleError(error));
    }

    public searchBooks(keywords: String): Promise<Book[]> {
        return this.http.get(`${BACKEND_URL}/books?keywords=${keywords}`, {headers: this.getHeadersAcceptJson()}).toPromise()
            .then(response => response.json() as Book[])
            .catch(error => this.handleError(error));
    }
}
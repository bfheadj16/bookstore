import {Injectable} from "@angular/core";
import {Customer} from "../models/customer";
import {Router} from "@angular/router";
import {BaseService, BACKEND_URL} from "./base.service";
import {Http, Headers} from "@angular/http";
import {NotificationsService} from "angular2-notifications";

const LOCALSTORAGE_KEY = 'customer';

@Injectable()
export class AuthService extends BaseService {
    private _customer: Customer = null;

    constructor(private router: Router, protected http: Http,
                private notificationService: NotificationsService) {
        super(http, notificationService);

        let data = window.localStorage.getItem(LOCALSTORAGE_KEY);
        if (data) {
            this._customer = JSON.parse(data);
        }
    }

    public updateLocalstorage() {
        window.localStorage.setItem(LOCALSTORAGE_KEY, JSON.stringify(this._customer));
    }

    public login(username: string, password: string): void {
        this._customer = new Customer();

        let headers = new Headers({"Accept": "text/plain"});
        headers.append('password', password);

        this.http.get(`${BACKEND_URL}/registrations/${username}`, {headers: headers}).toPromise()
            .then(response => {
                // Login ok, get user-data
                let custId = response.text();
                this.http.get(`${BACKEND_URL}/customers/${custId}`, {headers: this.getHeadersAcceptJson()}).toPromise()
                    .then(res => {
                        this._customer = res.json() as Customer;

                        // Save to local storage
                        this.updateLocalstorage();

                        this.router.navigate(['/catalog']);
                    })
                    .catch(error => this.handleError(error));
            })
            .catch(error => {
                this.logout();
                this.handleError(error)
            });
    }

    public logout(): void {
        this._customer = null;
        window.localStorage.removeItem(LOCALSTORAGE_KEY);
        this.router.navigate(['/catalog']);
    }

    public isLoggedIn(): boolean {
        return this._customer != null;
    }

    get customer(): Customer {
        return this._customer;
    }
}
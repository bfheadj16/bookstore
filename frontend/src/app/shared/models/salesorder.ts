import {Address} from './address';
import {CreditCard} from './creditcard';
import {SalesOrderItem} from './salesorder-item';

export class SalesOrder {
    number: number;
    date: string;
    amount: number;
    orderStatus: string;
    orderAddress: Address;
    orderCreditcard: CreditCard;
    salesOrderItems: SalesOrderItem[];
}
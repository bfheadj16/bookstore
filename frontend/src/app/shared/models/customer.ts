import {Address} from './address';
import {CreditCard} from './creditcard';
export class Customer {
    number: number;
    firstName: string;
    lastName: string;
    email: string;
    address: Address;
    creditCard: CreditCard;
}
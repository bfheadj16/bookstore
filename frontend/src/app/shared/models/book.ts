export class Book {
    constructor(readonly isbn: string,
                readonly title: string,
                readonly authors: string,
                readonly publisher: string,
                readonly publicationYear: number,
                readonly binding: BookBinding,
                readonly numberOfPages: number,
                readonly _price: number) {
    }

    get price() {
        return this._price;
    }
}

export enum BookBinding {
    HARDCOVER, PAPERBACK, EBOOK, UNKNOWN
}
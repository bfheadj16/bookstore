import {Book} from "./book";
export default class ShoppingCartItem {
    public bookInfo: Book;
    public book: Book;
    public price: number;
    public quantity: number;
}

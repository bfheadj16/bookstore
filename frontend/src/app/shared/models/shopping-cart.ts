import ShoppingCartItem from "./shopping-cart-item";
import {Book} from "./book";
import {NotificationsService} from 'angular2-notifications';

const LOCALSTORAGE_KEY = 'shoppingcart';

export default class ShoppingCart {
    public items: ShoppingCartItem[] = [];
    public totalPrice: number = 0;

    constructor(private notificationsService: NotificationsService) {
        let data = window.localStorage.getItem(LOCALSTORAGE_KEY);
        if (data) {
            this.items = JSON.parse(data);
            this.updateTotalPrice();
        }
    }

    public updateLocalstorage() {
        window.localStorage.setItem(LOCALSTORAGE_KEY, JSON.stringify(this.items));
    }

    public clear() {
        this.items = [];
        this.totalPrice = 0;
        this.updateLocalstorage();
    }

    public addBook(book: Book) {
        let foundBooks = this.items.filter(si => si.bookInfo.isbn === book.isbn);
        if (foundBooks && foundBooks.length > 0) {
            foundBooks[0].quantity++;
        } else {
            // Add new position
            let si = new ShoppingCartItem();
            si.bookInfo = book;
            si.quantity = 1;
            this.items.push(si);
        }

        this.notificationsService.success("Book added", `Book ${book.title} was added to your cart`);

        this.updateLocalstorage();
        this.updateTotalPrice();
    }

    public removeBook(book: Book) {
        let idx: number = -1;

        this.items.forEach((i, ix) => {
            if (i.bookInfo.isbn === book.isbn) {
                if (i.quantity > 1) {
                    i.quantity--;
                } else {
                    idx = ix;
                }
            }
        });

        if (idx > -1) {
            this.items.splice(idx, 1);
        }

        this.updateLocalstorage();
        this.updateTotalPrice();
    }

    public updateTotalPrice() {
        let price = 0;
        this.items.forEach(b => {
            price += b.bookInfo.price * b.quantity;
        });

        this.totalPrice = price;
    }

    public getTotalItems(): number {
        let itemCount = 0;
        this.items.forEach(b => {
            itemCount += b.quantity;
        });

        return itemCount;
    }
}
"use strict";
var shopping_cart_item_1 = require("./shopping-cart-item");
var ShoppingCart = (function () {
    function ShoppingCart(notificationsService) {
        this.notificationsService = notificationsService;
        this.items = [];
        this.totalPrice = 0;
    }
    ShoppingCart.prototype.clear = function () {
        this.items = [];
        this.totalPrice = 0;
    };
    ShoppingCart.prototype.addBook = function (book) {
        var foundBooks = this.items.filter(function (si) { return si.bookInfo.isbn === book.isbn; });
        if (foundBooks && foundBooks.length > 0) {
            foundBooks[0].quantity++;
        }
        else {
            // Add new position
            var si = new shopping_cart_item_1.default();
            si.bookInfo = book;
            si.quantity = 1;
            this.items.push(si);
        }
        this.notificationsService.success("Book added", "Book " + book.title + " was added to your cart");
        this.updateTotalPrice();
    };
    ShoppingCart.prototype.removeBook = function (book) {
        var idx = -1;
        this.items.forEach(function (i, ix) {
            if (i.bookInfo.isbn === book.isbn) {
                if (i.quantity > 1) {
                    i.quantity--;
                }
                else {
                    idx = ix;
                }
            }
        });
        if (idx > -1) {
            this.items.splice(idx, 1);
        }
        this.updateTotalPrice();
    };
    ShoppingCart.prototype.updateTotalPrice = function () {
        var price = 0;
        this.items.forEach(function (b) {
            price += b.bookInfo.price * b.quantity;
        });
        this.totalPrice = price;
    };
    ShoppingCart.prototype.getTotalItems = function () {
        var itemCount = 0;
        this.items.forEach(function (b) {
            itemCount += b.quantity;
        });
        return itemCount;
    };
    return ShoppingCart;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = ShoppingCart;
//# sourceMappingURL=shopping-cart.js.map
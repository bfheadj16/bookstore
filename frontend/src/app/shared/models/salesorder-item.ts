import ShoppingCartItem from './shopping-cart-item';
export class SalesOrderItem extends ShoppingCartItem {
    id: number;
    price: number;
}

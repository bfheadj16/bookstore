export class Address {
    street: string;
    city: string;
    postalCode: string;
    country: string;
    state: string;
}
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var shopping_cart_item_1 = require('./shopping-cart-item');
var SalesOrderItem = (function (_super) {
    __extends(SalesOrderItem, _super);
    function SalesOrderItem() {
        _super.apply(this, arguments);
    }
    return SalesOrderItem;
}(shopping_cart_item_1.default));
exports.SalesOrderItem = SalesOrderItem;
//# sourceMappingURL=salesorder-item.js.map
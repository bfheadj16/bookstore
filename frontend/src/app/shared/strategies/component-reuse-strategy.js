"use strict";
var ComponentReuseStrategy = (function () {
    function ComponentReuseStrategy() {
        this.handlers = {};
    }
    // Determines if a route (and its subtree) should be reattached
    ComponentReuseStrategy.prototype.shouldAttach = function (route) {
        return !!route.routeConfig && !!this.handlers[route.routeConfig.path];
    };
    // Determines if a route (and its subtree) should be detached to be reused later
    ComponentReuseStrategy.prototype.shouldDetach = function (route) {
        return route.routeConfig.path === "catalog";
    };
    // Determines if a route should be reused
    ComponentReuseStrategy.prototype.shouldReuseRoute = function (future, current) {
        return future.routeConfig === current.routeConfig;
    };
    // Stores a detached route
    ComponentReuseStrategy.prototype.store = function (route, handle) {
        this.handlers[route.routeConfig.path] = handle;
    };
    // Retrieves a previously stored route
    ComponentReuseStrategy.prototype.retrieve = function (route) {
        if (!route.routeConfig)
            return null;
        return this.handlers[route.routeConfig.path];
    };
    return ComponentReuseStrategy;
}());
exports.ComponentReuseStrategy = ComponentReuseStrategy;
//# sourceMappingURL=component-reuse-strategy.js.map
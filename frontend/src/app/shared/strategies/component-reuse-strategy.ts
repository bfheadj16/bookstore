import {RouteReuseStrategy, ActivatedRouteSnapshot, DetachedRouteHandle} from "@angular/router";

export class ComponentReuseStrategy implements RouteReuseStrategy {

    private handlers: {[key: string]: DetachedRouteHandle} = {};

    // Determines if a route (and its subtree) should be reattached
    public shouldAttach(route: ActivatedRouteSnapshot): boolean {
        return !!route.routeConfig && !!this.handlers[route.routeConfig.path];
    }

    // Determines if a route (and its subtree) should be detached to be reused later
    public shouldDetach(route: ActivatedRouteSnapshot): boolean {
        return route.routeConfig.path === "catalog";
    }

    // Determines if a route should be reused
    public shouldReuseRoute(future: ActivatedRouteSnapshot, current: ActivatedRouteSnapshot): boolean {
        return future.routeConfig === current.routeConfig;
    }

    // Stores a detached route
    public store(route: ActivatedRouteSnapshot, handle: DetachedRouteHandle): void {
        this.handlers[route.routeConfig.path] = handle;
    }

    // Retrieves a previously stored route
    public retrieve(route: ActivatedRouteSnapshot): DetachedRouteHandle {
        if (!route.routeConfig) return null;
        return this.handlers[route.routeConfig.path];
    }
}
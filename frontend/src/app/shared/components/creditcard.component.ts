import {Component, Input, ViewChild} from '@angular/core';
import {CreditCard} from '../models/creditcard';
import {NgForm} from '@angular/forms';
import {CustomerService} from '../services/customer.service';

@Component({
    selector: 'customer-creditcard',
    templateUrl: './creditcard.component.html'
})
export class CreditcardComponent {
    @Input() creditCard: CreditCard;
    @Input() editable: boolean;
    @Input() directUpdate: boolean;
    @ViewChild('creditcardForm') form: NgForm;

    public creditCardTypes = ['MASTERCARD', 'VISA'];

    constructor(private customerService: CustomerService){}

    updateCreditcard() {
        if (!this.editable) {
            return;
        }

        this.customerService.updateCustomerData();
    }
}

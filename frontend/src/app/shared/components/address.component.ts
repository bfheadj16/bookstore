import {Component, Input, ViewChild} from '@angular/core';
import {Address} from '../models/address';
import {NgForm} from '@angular/forms';
import {CustomerService} from '../services/customer.service';

@Component({
    selector: 'customer-address',
    templateUrl: './address.component.html'
})
export class AddressComponent {
    @Input() address: Address;
    @Input() editable: boolean;
    @Input() directUpdate: boolean;
    @ViewChild('addressForm') form: NgForm;

    constructor(private customerService: CustomerService) {
    }

    updateAddress() {
        if (!this.editable) {
            return;
        }

        this.customerService.updateCustomerData();
    }
}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var address_1 = require('../models/address');
var forms_1 = require('@angular/forms');
var customer_service_1 = require('../services/customer.service');
var AddressComponent = (function () {
    function AddressComponent(customerService) {
        this.customerService = customerService;
    }
    AddressComponent.prototype.updateAddress = function () {
        if (!this.editable) {
            return;
        }
        this.customerService.updateCustomerData();
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', address_1.Address)
    ], AddressComponent.prototype, "address", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], AddressComponent.prototype, "editable", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], AddressComponent.prototype, "directUpdate", void 0);
    __decorate([
        core_1.ViewChild('addressForm'), 
        __metadata('design:type', forms_1.NgForm)
    ], AddressComponent.prototype, "form", void 0);
    AddressComponent = __decorate([
        core_1.Component({
            selector: 'customer-address',
            templateUrl: '../..//shared/components/address.component.html'
        }), 
        __metadata('design:paramtypes', [customer_service_1.CustomerService])
    ], AddressComponent);
    return AddressComponent;
}());
exports.AddressComponent = AddressComponent;
//# sourceMappingURL=address.component.js.map

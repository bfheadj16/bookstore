import {Component, Input, ViewChild} from "@angular/core";
import {Customer} from '../models/customer';
import {NgForm} from '@angular/forms';

@Component({
    selector: 'customer-info',
    templateUrl: './customerinfo.component.html'
})
export class CustomerInfoComponent {
    @Input() editable: boolean;
    @Input() customer: Customer;
    @ViewChild('customerInfoForm') form: NgForm;
}

import {Component} from '@angular/core';
import {OrderService} from '../shared/services/order.service';
@Component({
    selector: 'shopping-cart',
    templateUrl: './shopping-cart.component.html'
})
export class ShoppingCartComponent {
    constructor(private orderService: OrderService) {
    }
}

import {Component} from "@angular/core";
import {OrderService} from './shared/services/order.service';
import {AuthService} from './shared/services/auth.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent {
    notificationOptions = {
        position: ['top', 'right'],
        timeOut: 3000,
        showProgressBar: true
    };

    public open: boolean;

    constructor(public orderService: OrderService, public authService: AuthService){}

    toggleOpen() {
        this.open = !this.open;
    }

    logout() {
        this.open = false;
        this.authService.logout();
    }
}

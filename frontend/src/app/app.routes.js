"use strict";
var shopping_cart_component_1 = require('./shopping-cart/shopping-cart.component.ts');
var catalog_component_1 = require('./catalog/catalog.component.ts');
var book_detail_component_1 = require('./book/book-detail.component.ts');
var customer_registration_component_1 = require('./customer/customer-registration.component.ts');
var order_summary_component_1 = require("./order/order-summary.component.ts");
var customer_login_component_1 = require('./customer/customer-login.component.ts');
var auth_guard_1 = require('./shared/guards/auth-guard');
var customer_account_component_1 = require('./customer/customer-account.component.ts');
var order_details_component_1 = require("./order/order-details.component.ts");
var customer_edit_component_1 = require('./customer/customer-edit.component.ts');
var order_confirmation_1 = require("./order/order-confirmation");
exports.appRoutes = [
    { path: 'catalog', component: catalog_component_1.CatalogComponent },
    { path: 'cart', component: shopping_cart_component_1.ShoppingCartComponent },
    { path: 'register', component: customer_registration_component_1.UserRegistrationComponent },
    { path: 'login', component: customer_login_component_1.UserLoginComponent },
    { path: 'books/:isbn', component: book_detail_component_1.BookDetailComponent },
    { path: 'account', component: customer_account_component_1.UserAccountComponent, canActivate: [auth_guard_1.AuthGuard] },
    { path: 'account/edit', component: customer_edit_component_1.UserEditComponent, canActivate: [auth_guard_1.AuthGuard] },
    { path: 'orderdetails/:ordernumber', component: order_details_component_1.OrderDetailsComponent, canActivate: [auth_guard_1.AuthGuard] },
    { path: 'orderconfirmation/:ordernumber', component: order_confirmation_1.OrderConfirmationComponent, canActivate: [auth_guard_1.AuthGuard] },
    { path: 'checkout', component: order_summary_component_1.OrderSummaryComponent, canActivate: [auth_guard_1.AuthGuard] },
    {
        path: '',
        redirectTo: '/catalog',
        pathMatch: 'full'
    }
];
//# sourceMappingURL=app.routes.js.map

import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute} from '@angular/router';
import {Book} from '../shared/models/book';
import {CatalogService} from '../shared/services/catalog.service';
import {OrderService} from '../shared/services/order.service';

@Component({
    selector: 'book-detail',
    templateUrl: './book-detail.component.html'
})
export class BookDetailComponent implements OnInit {
    constructor(private orderService: OrderService,
                private catalog: CatalogService,
                private route: ActivatedRoute,
                private location: Location) { }

    public book: Book;

    ngOnInit(): void {
        let isbn = this.route.snapshot.params['isbn'];

        // Find bookInfo
        this.catalog.findBook(isbn).then(book => this.book = book);
    }

    public addToCart(book: Book) {
        this.orderService.cart.addBook(book);
    }

    public goBack(): void {
        this.location.back();
    }
}

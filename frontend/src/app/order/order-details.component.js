"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var customer_service_1 = require('../shared/services/customer.service.ts');
var order_service_1 = require('../shared/services/order.service.ts');
var OrderDetailsComponent = (function () {
    function OrderDetailsComponent(userService, route, orderService) {
        var _this = this;
        this.userService = userService;
        this.route = route;
        this.orderService = orderService;
        var orderNumber = parseInt(this.route.snapshot.params['ordernumber']);
        userService.getOrderByNumber(orderNumber)
            .then(function (so) { return _this.order = so; });
    }
    OrderDetailsComponent.prototype.cancelOrder = function () {
        var _this = this;
        this.orderService.cancelOrder(this.order.number)
            .then(function () { return _this.userService.getOrderByNumber(_this.order.number)
            .then(function (so) { return _this.order = so; }); });
    };
    OrderDetailsComponent = __decorate([
        core_1.Component({
            selector: 'order-summary',
            templateUrl: 'order-details.component.html'
        }), 
        __metadata('design:paramtypes', [customer_service_1.CustomerService, router_1.ActivatedRoute, order_service_1.OrderService])
    ], OrderDetailsComponent);
    return OrderDetailsComponent;
}());
exports.OrderDetailsComponent = OrderDetailsComponent;
//# sourceMappingURL=order-details.component.js.map

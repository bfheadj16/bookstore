"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var order_service_1 = require("../shared/services/order.service.ts");
var auth_service_1 = require("../shared/services/auth.service.ts");
var router_1 = require("@angular/router");
var OrderSummaryComponent = (function () {
    function OrderSummaryComponent(router, orderService, authService) {
        this.router = router;
        this.orderService = orderService;
        this.authService = authService;
    }
    OrderSummaryComponent.prototype.order = function () {
        var _this = this;
        this.orderService.order().then(function (o) { return _this.router.navigate([("orderconfirmation/" + o.number)]); });
    };
    OrderSummaryComponent = __decorate([
        core_1.Component({
            selector: 'order-summary',
            templateUrl: 'order-summary.component.html'
        }), 
        __metadata('design:paramtypes', [router_1.Router, order_service_1.OrderService, auth_service_1.AuthService])
    ], OrderSummaryComponent);
    return OrderSummaryComponent;
}());
exports.OrderSummaryComponent = OrderSummaryComponent;
//# sourceMappingURL=order-summary.component.js.map

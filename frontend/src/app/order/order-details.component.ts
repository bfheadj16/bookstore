import {Component} from '@angular/core';
import {SalesOrder} from '../shared/models/salesorder';
import {ActivatedRoute} from '@angular/router';
import {CustomerService} from '../shared/services/customer.service';
import {OrderService} from '../shared/services/order.service';

@Component({
    selector: 'order-summary',
    templateUrl: './order-details.component.html'
})
export class OrderDetailsComponent {

    public order: SalesOrder;

    constructor(private userService: CustomerService, private route: ActivatedRoute,
                private orderService: OrderService) {

        let orderNumber = parseInt(this.route.snapshot.params['ordernumber']);
        userService.getOrderByNumber(orderNumber)
            .then(so => this.order = so);
    }

    cancelOrder() {
        this.orderService.cancelOrder(this.order.number)
            .then(() => this.userService.getOrderByNumber(this.order.number)
            .then(so => this.order = so));
    }
}

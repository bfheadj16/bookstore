"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var OrderItemListComponent = (function () {
    function OrderItemListComponent() {
        this.items = [];
    }
    OrderItemListComponent.prototype.calcTotalPrice = function () {
        var price = 0;
        this.items.forEach(function (b) {
            if (b.bookInfo) {
                price += b.bookInfo.price * b.quantity;
            }
            else {
                price += b.price * b.quantity;
            }
        });
        return price;
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], OrderItemListComponent.prototype, "items", void 0);
    OrderItemListComponent = __decorate([
        core_1.Component({
            selector: 'order-item-list',
            templateUrl: 'order-item-list.component.html'
        }), 
        __metadata('design:paramtypes', [])
    ], OrderItemListComponent);
    return OrderItemListComponent;
}());
exports.OrderItemListComponent = OrderItemListComponent;
//# sourceMappingURL=order-item-list.component.js.map

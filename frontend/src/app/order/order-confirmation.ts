
import {Component} from "@angular/core";
import {OrderService} from "../shared/services/order.service";
import {AuthService} from "../shared/services/auth.service";
import {ActivatedRoute} from "@angular/router";
@Component({
    selector: 'order-confirmation',
    templateUrl: './order-confirmation.component.html'
})
export class OrderConfirmationComponent {
    public orderNumber: Number;
    constructor(private orderService: OrderService, private route: ActivatedRoute) {
        this.orderNumber = parseInt(this.route.snapshot.params['ordernumber']);
    }
}

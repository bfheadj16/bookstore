import {Component, Input} from '@angular/core';
import ShoppingCartItem from '../shared/models/shopping-cart-item';
@Component({
    selector: 'order-item-list',
    templateUrl: './order-item-list.component.html'
})
export class OrderItemListComponent {
    @Input()
    public items: ShoppingCartItem[] = [];

    public calcTotalPrice() {
        let price = 0;
        this.items.forEach(b => {
            if (b.bookInfo) {
                price += b.bookInfo.price * b.quantity;
            } else {
                price += b.price * b.quantity;
            }
        });

        return price;
    }
}


import {Component} from "@angular/core";
import {OrderService} from "../shared/services/order.service";
import {AuthService} from "../shared/services/auth.service";
import {Router} from "@angular/router";
@Component({
    selector: 'order-summary',
    templateUrl: './order-summary.component.html'
})
export class OrderSummaryComponent {

    public processingRunning = false;

    constructor(private router: Router, private orderService: OrderService, private authService: AuthService) {
    }

    public order() {
        this.processingRunning = true;
        this.orderService.order()
            .then(o => this.router.navigate([`orderconfirmation/${o.number}`]))
            .catch(o => this.processingRunning = false);
    }
}

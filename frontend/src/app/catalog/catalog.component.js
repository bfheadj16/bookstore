"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var catalog_service_1 = require("../shared/services/catalog.service.ts");
var order_service_1 = require('../shared/services/order.service.ts');
var CatalogComponent = (function () {
    function CatalogComponent(catalog, router, orderService) {
        this.catalog = catalog;
        this.router = router;
        this.orderService = orderService;
    }
    CatalogComponent.prototype.search = function () {
        var _this = this;
        this.catalog.searchBooks(this.searchKeyWords)
            .then(function (books) { return _this.books = books; });
    };
    CatalogComponent.prototype.showDetails = function (isbn) {
        this.router.navigate(['/books', isbn]);
    };
    CatalogComponent.prototype.addToCart = function ($event, book) {
        this.orderService.cart.addBook(book);
        // Stop from navigating to details
        $event.stopImmediatePropagation();
        $event.stopPropagation();
    };
    CatalogComponent = __decorate([
        core_1.Component({
            selector: 'catalog',
            templateUrl: 'catalog.component.html'
        }), 
        __metadata('design:paramtypes', [catalog_service_1.CatalogService, router_1.Router, order_service_1.OrderService])
    ], CatalogComponent);
    return CatalogComponent;
}());
exports.CatalogComponent = CatalogComponent;
//# sourceMappingURL=catalog.component.js.map

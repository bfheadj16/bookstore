import {Component} from "@angular/core";
import {Book} from "../shared/models/book";
import {Router} from "@angular/router";
import {CatalogService} from "../shared/services/catalog.service";
import {OrderService} from '../shared/services/order.service';

@Component({
    selector: 'catalog',
    templateUrl: './catalog.component.html'
})
export class CatalogComponent {
    public searchKeyWords: string;

    public books: Book[];

    public showSpinner = false;

    constructor(private catalog: CatalogService, private router: Router,
                private orderService: OrderService) {
    }

    public search() {
        this.showSpinner = true;
        this.catalog.searchBooks(this.searchKeyWords)
            .then(books => {
                this.showSpinner = false;
                this.books = books;
            })
            .catch(e => this.showSpinner = false);
    }

    public showDetails(isbn: string) {
        this.router.navigate(['/books', isbn]);
    }

    public addToCart($event: any, book: Book) {
        this.orderService.cart.addBook(book);

        // Stop from navigating to details
        $event.stopImmediatePropagation();
        $event.stopPropagation();
    }
}

import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {ShoppingCartComponent} from './shopping-cart/shopping-cart.component';
import {RouterModule, RouteReuseStrategy} from '@angular/router';
import {appRoutes} from './app.routes';
import {CatalogComponent} from './catalog/catalog.component';
import {BookDetailComponent} from './book/book-detail.component';
import {CatalogService} from './shared/services/catalog.service';
import {OrderService} from './shared/services/order.service';
import {UserRegistrationComponent} from './customer/customer-registration.component';
import {AddressComponent} from './shared/components/address.component';
import {CreditcardComponent} from './shared/components/creditcard.component';
import {OrderSummaryComponent} from './order/order-summary.component';
import {OrderItemListComponent} from './order/order-item-list.component';
import {SimpleNotificationsModule} from 'angular2-notifications';
import {AuthService} from './shared/services/auth.service';
import {UserLoginComponent} from './customer/customer-login.component';
import {AuthGuard} from './shared/guards/auth-guard';
import {CustomerService} from './shared/services/customer.service';
import {UserAccountComponent} from './customer/customer-account.component';
import {OrderDetailsComponent} from './order/order-details.component';
import {UserEditComponent} from './customer/customer-edit.component';
import {CustomerInfoComponent} from './shared/components/customerinfo.component';
import {ComponentReuseStrategy} from './shared/strategies/component-reuse-strategy';
import {HttpModule} from "@angular/http";
import {OrderConfirmationComponent} from "./order/order-confirmation";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        RouterModule.forRoot(appRoutes),
        SimpleNotificationsModule.forRoot()],

    providers: [OrderService,
        CatalogService,
        AuthService,
        CustomerService,
        AuthGuard,
        {provide: RouteReuseStrategy, useClass: ComponentReuseStrategy}],

    declarations: [AppComponent,
        ShoppingCartComponent,
        CatalogComponent,
        BookDetailComponent,
        AddressComponent,
        UserRegistrationComponent,
        UserLoginComponent,
        OrderItemListComponent,
        OrderSummaryComponent,
        OrderConfirmationComponent,
        UserAccountComponent,
        OrderDetailsComponent,
        UserEditComponent,
        CustomerInfoComponent,
        CreditcardComponent],

    bootstrap: [AppComponent]
})
export class AppModule {
}
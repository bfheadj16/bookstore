import {Component} from '@angular/core';
import {CustomerService} from '../shared/services/customer.service';
import {AuthService} from '../shared/services/auth.service';

@Component({
    selector: 'user-edit',
    templateUrl: './customer-edit.component.html'
})
export class UserEditComponent{
    constructor(public authService: AuthService, private customerService: CustomerService) {
    }

    updateCustomerData() {
        this.customerService.updateCustomerData();
    }
}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var customer_1 = require('../shared/models/customer');
var creditcard_1 = require('../shared/models/creditcard');
var address_1 = require('../shared/models/address');
var customer_service_1 = require('../shared/services/customer.service.ts');
var auth_service_1 = require('../shared/services/auth.service.ts');
var UserRegistrationComponent = (function () {
    function UserRegistrationComponent(customerService, authService) {
        this.customerService = customerService;
        this.authService = authService;
        this.customer = new customer_1.Customer();
        this.customer.address = new address_1.Address();
        this.customer.creditCard = new creditcard_1.CreditCard();
    }
    UserRegistrationComponent.prototype.registerCustomer = function () {
        var _this = this;
        this.customerService.registerCustomer({ customer: this.customer, password: this.password })
            .then(function () { return _this.authService.login(_this.customer.email, _this.password); });
    };
    UserRegistrationComponent = __decorate([
        core_1.Component({
            selector: 'user-registration',
            templateUrl: '/bookstore/src/app/customer/customer-registration.component.html'
        }), 
        __metadata('design:paramtypes', [customer_service_1.CustomerService, auth_service_1.AuthService])
    ], UserRegistrationComponent);
    return UserRegistrationComponent;
}());
exports.UserRegistrationComponent = UserRegistrationComponent;
//# sourceMappingURL=customer-registration.component.js.map

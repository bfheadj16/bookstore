import {AuthService} from '../shared/services/auth.service';
import {Component} from '@angular/core';

@Component({
    selector: 'user-login',
    templateUrl: './customer-login.component.html'
})
export class UserLoginComponent{
    public username: string = 'adrian.bader@students.bfh.ch';
    public password: string = '123';

    constructor(private authService: AuthService) {}

    login() {
        this.authService.login(this.username, this.password);
    }
}

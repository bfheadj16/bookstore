import {Component} from '@angular/core';
import {CustomerService} from '../shared/services/customer.service';
import {SalesOrder} from '../shared/models/salesorder';
import {AuthService} from '../shared/services/auth.service';
import {Router} from '@angular/router';
@Component({
    selector: 'user-account',
    templateUrl: './customer-account.component.html'
})
export class UserAccountComponent {
    public year: number;
    public orders: SalesOrder[];

    constructor(private userService: CustomerService,
                public authService: AuthService, private router: Router) {
        this.year = 2017;
    }

    getOrders() {
        this.userService.getOrdersByYear(this.year)
            .then(orders => this.orders = orders);
    }

    orderDetails(number: number) {
        this.router.navigate(['/orderdetails/' + number]);
    }
}

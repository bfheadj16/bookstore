import {Component} from '@angular/core';
import {Customer} from '../shared/models/customer';
import {CreditCard} from '../shared/models/creditcard';
import {Address} from '../shared/models/address';
import {CustomerService} from '../shared/services/customer.service';
import {AuthService} from '../shared/services/auth.service';

@Component({
    selector: 'user-registration',
    templateUrl: './customer-registration.component.html'
})
export class UserRegistrationComponent {
    public customer: Customer;
    public password: string;

    constructor(private customerService: CustomerService, private authService: AuthService) {
        this.customer = new Customer();
        this.customer.address = new Address();
        this.customer.creditCard = new CreditCard();
    }

    registerCustomer() {
        this.customerService.registerCustomer({customer: this.customer, password: this.password})
            .then(() => this.authService.login(this.customer.email, this.password));
    }
}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var customer_service_1 = require('../shared/services/customer.service.ts');
var auth_service_1 = require('../shared/services/auth.service.ts');
var router_1 = require('@angular/router');
var UserAccountComponent = (function () {
    function UserAccountComponent(userService, authService, router) {
        this.userService = userService;
        this.authService = authService;
        this.router = router;
        this.year = 2017;
    }
    UserAccountComponent.prototype.getOrders = function () {
        var _this = this;
        this.userService.getOrdersByYear(this.year)
            .then(function (orders) { return _this.orders = orders; });
    };
    UserAccountComponent.prototype.orderDetails = function (number) {
        this.router.navigate(['/orderdetails/' + number]);
    };
    UserAccountComponent = __decorate([
        core_1.Component({
            selector: 'user-account',
            templateUrl: '/bookstore/src/app/customer/customer-account.component.html'
        }), 
        __metadata('design:paramtypes', [customer_service_1.CustomerService, auth_service_1.AuthService, router_1.Router])
    ], UserAccountComponent);
    return UserAccountComponent;
}());
exports.UserAccountComponent = UserAccountComponent;
//# sourceMappingURL=customer-account.component.js.map

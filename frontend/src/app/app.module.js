"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var platform_browser_1 = require('@angular/platform-browser');
var app_component_1 = require('./app.component.ts');
var shopping_cart_component_1 = require('./shopping-cart/shopping-cart.component.ts');
var router_1 = require('@angular/router');
var app_routes_1 = require('./app.routes.ts');
var catalog_component_1 = require('./catalog/catalog.component.ts');
var book_detail_component_1 = require('./book/book-detail.component.ts');
var catalog_service_1 = require('./shared/services/catalog.service.ts');
var order_service_1 = require('./shared/services/order.service.ts');
var customer_registration_component_1 = require('./customer/customer-registration.component.ts');
var address_component_1 = require('./shared/components/address.component.ts');
var creditcard_component_1 = require('./shared/components/creditcard.component.ts');
var order_summary_component_1 = require('./order/order-summary.component.ts');
var order_item_list_component_1 = require('./order/order-item-list.component.ts');
var angular2_notifications_1 = require('angular2-notifications');
var auth_service_1 = require('./shared/services/auth.service.ts');
var customer_login_component_1 = require('./customer/customer-login.component.ts');
var auth_guard_1 = require('./shared/guards/auth-guard');
var customer_service_1 = require('./shared/services/customer.service.ts');
var customer_account_component_1 = require('./customer/customer-account.component.ts');
var order_details_component_1 = require('./order/order-details.component.ts');
var customer_edit_component_1 = require('./customer/customer-edit.component.ts');
var customerinfo_component_1 = require('./shared/components/customerinfo.component.ts');
var component_reuse_strategy_1 = require('./shared/strategies/component-reuse-strategy');
var http_1 = require("@angular/http");
var order_confirmation_1 = require("./order/order-confirmation");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                http_1.HttpModule,
                router_1.RouterModule.forRoot(app_routes_1.appRoutes),
                angular2_notifications_1.SimpleNotificationsModule.forRoot()],
            providers: [order_service_1.OrderService,
                catalog_service_1.CatalogService,
                auth_service_1.AuthService,
                customer_service_1.CustomerService,
                auth_guard_1.AuthGuard,
                { provide: router_1.RouteReuseStrategy, useClass: component_reuse_strategy_1.ComponentReuseStrategy }],
            declarations: [app_component_1.AppComponent,
                shopping_cart_component_1.ShoppingCartComponent,
                catalog_component_1.CatalogComponent,
                book_detail_component_1.BookDetailComponent,
                address_component_1.AddressComponent,
                customer_registration_component_1.UserRegistrationComponent,
                customer_login_component_1.UserLoginComponent,
                order_item_list_component_1.OrderItemListComponent,
                order_summary_component_1.OrderSummaryComponent,
                order_confirmation_1.OrderConfirmationComponent,
                customer_account_component_1.UserAccountComponent,
                order_details_component_1.OrderDetailsComponent,
                customer_edit_component_1.UserEditComponent,
                customerinfo_component_1.CustomerInfoComponent,
                creditcard_component_1.CreditcardComponent],
            bootstrap: [app_component_1.AppComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map

import {Routes} from '@angular/router';
import {ShoppingCartComponent} from './shopping-cart/shopping-cart.component';
import {CatalogComponent} from './catalog/catalog.component';
import {BookDetailComponent} from './book/book-detail.component';
import {UserRegistrationComponent} from './customer/customer-registration.component';
import {OrderSummaryComponent} from "./order/order-summary.component";
import {UserLoginComponent} from './customer/customer-login.component';
import {AuthGuard} from './shared/guards/auth-guard';
import {UserAccountComponent} from './customer/customer-account.component';
import {OrderDetailsComponent} from "./order/order-details.component";
import {UserEditComponent} from './customer/customer-edit.component';
import {OrderConfirmationComponent} from "./order/order-confirmation";

export const appRoutes: Routes = [
    {path: 'catalog', component: CatalogComponent},
    {path: 'cart', component: ShoppingCartComponent},
    {path: 'register', component: UserRegistrationComponent},
    {path: 'login', component: UserLoginComponent},
    {path: 'books/:isbn', component: BookDetailComponent},
    {path: 'account', component: UserAccountComponent, canActivate: [AuthGuard]},
    {path: 'account/edit', component: UserEditComponent, canActivate: [AuthGuard]},
    {path: 'orderdetails/:ordernumber', component: OrderDetailsComponent, canActivate: [AuthGuard]},
    {path: 'orderconfirmation/:ordernumber', component: OrderConfirmationComponent, canActivate: [AuthGuard]},
    {path: 'checkout', component: OrderSummaryComponent, canActivate: [AuthGuard]},
    {
        path: '**',
        redirectTo: '/catalog',
        pathMatch: 'full'
    }
];